# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys
from dict_constants import ENTREZ_GENE, IMGT, OMIM, MIRBASE,\
    VEGA, RGD, HGNC, ENSEMBL, HPRD, XREF_FROM_DB, ID, GENE_DB_NAME,\
    HISTORIC, VER, DICT_COLL, DICT_TERM, DICT_SYMBOL, COL_NCBI_FILE_tax_id,\
    COL_NCBI_FILE_gene_id, COL_NCBI_FILE_symbol, COL_NCBI_FILE_synonyms,\
    COL_NCBI_FILE_description, COL_NCBI_FILE_symbol_from_nomen,\
    COL_NCBI_FILE_full_from_nomen, COL_NCBI_FILE_other_designations
    
current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection
from progressbar import ProgressBar, Percentage, Bar
# Cross-references
#  IMGT/GENE-DB
#  MIM
#  miRBase
#  Vega
#  RGD
#  HGNC
#  Ensembl
#  HPRD

def create_document(fields):
    doc = {}
    tax_id = fields[COL_NCBI_FILE_tax_id]
    gene_id = fields[COL_NCBI_FILE_gene_id]
    symbol = fields[COL_NCBI_FILE_symbol]
    synonyms = fields[COL_NCBI_FILE_synonyms]            # pipe-delimited set OR '-'
    description = fields[COL_NCBI_FILE_description]
    symbol_from_nomen = fields[COL_NCBI_FILE_symbol_from_nomen]  # '-'
    full_from_nomen = fields[COL_NCBI_FILE_full_from_nomen]    # '-'
    other_designations = fields[COL_NCBI_FILE_other_designations] # pipe-delimited set OR '-' 
    
    doc["tax_id"] = tax_id
    doc[ENTREZ_GENE + "_id"] = gene_id
    doc["symbol"] = symbol
    if synonyms != "-":
        doc["synonyms"] = synonyms.split("|")
    if description != "-":
        doc["description"] = description
    if symbol_from_nomen != "-":
        doc["symbol_from_nomenclature"] = symbol_from_nomen
    if full_from_nomen != "-":
        doc["full_from_nomenclature"] = full_from_nomen
    if other_designations != "-":
        doc["other_designations"] = other_designations.split("|")
    return doc

def cross_references(fields):
    gene_id = fields[1]
    dbxrefs = fields[5]             # pipe-delimited set OR '-'
    if not dbxrefs == "-":
        record = {}
        record[ENTREZ_GENE + ID] = gene_id
        xrefs = dbxrefs.split("|")
        for xref in xrefs:                
            m = xref.split(":")
            tag = get_db_tag(m[0])
            if record.has_key(tag + ID):
                m[1] = record[tag + ID] + "|" + m[1]
            record[tag + ID] = m[1]            
        return record
    return None

def extract_human_taxid(gene_file_name, file_9606):
    
    for l in file(gene_file_name):
        fields = l.strip().split("\t")
        if fields[0] == "9606":
            file_9606.write(l)
    file_9606.close()

def main(gene_file_name, header, gene_file_history, header_history):
    ###############        
    ## GENE INFO ##
    ###############
    print "###############"
    print "# NCBI Source #"
    print "###############"
    print ""
    print "Terminology extraction from", gene_file_name + "..."
    
    file_len = len(open(gene_file_name).readlines())
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    gene_connection = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + VER)
    gene_connection.delete_collection()
    gene_xref = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + XREF_FROM_DB + VER)
    gene_xref.delete_collection()
    
    for l in file(gene_file_name):
        progvar += 1
        progress.update(progvar)
        if header:
            header = 0
            continue
        fields = l.strip().split("\t")
        if fields[0] == "9606":        
            # NCBI FIELDS 
            doc = create_document(fields)            
            gene_connection.insert_document(doc)
            # CROSS-REFERENCES 
            doc = cross_references(fields)
            if doc:
                gene_xref.insert_document(doc)
            
    gene_connection.close()         
    
    progress.finish()
    print "Done!"
    print "Two new collections added in \"" +GENE_DB_NAME + "\" database:"
    print "    -", ENTREZ_GENE + VER, "with",gene_connection.find().count(), "rows"
    print "    -", ENTREZ_GENE + XREF_FROM_DB + VER, "with",gene_xref.find().count(), "rows"
    
    ##################        
    ## GENE HISTORY ##
    ##################
    #gene_file_name = IN_DIR + ENTREZ_FILE_HISTORIC
    print ""
    print "History extraction from file..."
    print gene_file_history
    
    file_len = len(open(gene_file_history).readlines())
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    gene_connection = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + HISTORIC + VER)
    gene_connection.delete_collection()
    
    for l in file(gene_file_history):
        progvar += 1
        progress.update(progvar)
        if header_history:
            header_history = 0
            continue
        fields = l.strip().split("\t")
        if fields[0] == "9606":
            doc = {}
            doc[ENTREZ_GENE + ID] = fields[1]
            doc[ENTREZ_GENE + HISTORIC] = fields[2]
            gene_connection.insert_document(doc)
       
    gene_connection.close()  
    progress.finish()
    
    print "Done!"
    print "One new collection added in \"" +GENE_DB_NAME + "\" database:"
    print "    -", ENTREZ_GENE + HISTORIC + VER, "with",gene_connection.find().count(), "rows"
    
def get_db_tag(name):
    if name == "IMGT/GENE-DB":
        return IMGT
    elif name == "MIM":
        return OMIM
    elif name == "miRBase":
        return MIRBASE
    elif name == "Vega":
        return VEGA
    elif name == "RGD":
        return RGD
    elif name == "HGNC":
        return HGNC
    elif name == "Ensembl":
        return ENSEMBL
    elif name == "HPRD":
        return HPRD
    else:
        print "ERROR!!! NO DB!!!"
        sys.exit()
        
def get_entry(iden, term, is_simbol):
    aux = {}
    aux[DICT_TERM] = term
    aux[ENTREZ_GENE + ID] = iden
    aux[DICT_SYMBOL] = is_simbol
    return aux

def name_extraction():
    print ""
    print "Raw dictionary creation..."
    
    gene_connection = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + VER)
    
    gene_dict_connection = MongoConnection(GENE_DB_NAME, ENTREZ_GENE + DICT_COLL + VER)
    gene_dict_connection.delete_collection()
    gene_dict_connection.create_index(ENTREZ_GENE + ID)
    
    cursor = gene_connection.find()
    file_len = cursor.count()
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    for record in cursor:
        progvar+=1
        progress.update(progvar)
        gene_id = record.get(ENTREZ_GENE + ID)
        
        symbol = record.get("symbol", None)
        if symbol:
            gene_dict_connection.insert_document(get_entry(gene_id, symbol, True))
            
        synonyms = record.get("synonyms", None)
        if synonyms:
            for s in synonyms:
                gene_dict_connection.insert_document(get_entry(gene_id, s, True))
        
        descriptions = record.get("description", None)
        if descriptions:
            gene_dict_connection.insert_document(get_entry(gene_id, descriptions, False))
            
        symbol_from = record.get("symbol_from_nomenclature", None)
        if symbol_from:
            gene_dict_connection.insert_document(get_entry(gene_id, symbol_from, True))
        
        full_from = record.get("full_from_nomenclature", None)
        if full_from:
            gene_dict_connection.insert_document(get_entry(gene_id, full_from, False))
        
        others = record.get("other_designations", None)
        if others:
            for o in others:
                gene_dict_connection.insert_document(get_entry(gene_id, o, False))
    
    progress.finish() 
    print "Done!" 
    print "One collection added in \"" +GENE_DB_NAME + "\" database:"
    print "    -",ENTREZ_GENE + DICT_COLL + VER, "with", gene_dict_connection.find().count(),"entries"
    
if __name__ == '__main__':
    ncbi_path = sys.argv[1]
    header = 0
    header_str = sys.argv[2].lower()
    if "1" in header_str or "t" in header_str or "y" in header_str:
        header = 1
    ncbi_history_path  = sys.argv[3]
    
    header_hist = 0
    header_str = sys.argv[4].lower()
    if "1" in header_str or "t" in header_str or "y" in header_str:
        header_hist = 1
    print ""
    
    main(ncbi_path, header, ncbi_history_path, header_hist)
    name_extraction()
    print ""