# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys
from dict_constants import DICT_DISEASE, DICT_DB_NAME,\
    DICT_ID, DICT_TERM, DICT_TUI, DICT_VER, DICT_PLURAL, \
    DICT_SYMBOL, VER, COL_UMLS_FILE_tty, COL_UMLS_FILE_str,\
    COL_UMLS_FILE_cui
    
current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection
from normalization import disease_normalization

def get_cuis_black_dict(cuis_black_list_path, header):
    cuis_black_dict = {}
    if cuis_black_list_path:
        for lin in file(cuis_black_list_path):
            if header:
                header=0
                continue
            lin_split = lin.strip().split("\t")
            if lin_split[0][0] == "#":
                continue
            cuis_black_dict[lin_split[0]] = 1
    return cuis_black_dict

def dictionary_creation(collection, path, header, acr_dict, cuis_black_dict):
    
    dict_coll = MongoConnection(DICT_DB_NAME, collection+ VER )
    dict_coll.delete_collection()
    dict_coll.create_index(DICT_ID)
    dict_coll.create_index(DICT_TERM)
    dict_coll.create_index(DICT_TUI)
    dict_coll.create_index(DICT_VER)
    
    cotrol_dict = {}
    
    for lin in file(path):
        
        if header:
            header = 0
            continue
        lin_split = unicode(lin.strip(), errors = 'replace').split("\t")
        
        if lin_split[0] in cuis_black_dict:
            continue
        
        doc = {}
        doc[DICT_ID] = lin_split[COL_UMLS_FILE_cui]
        doc[DICT_TERM] = lin_split[COL_UMLS_FILE_str]
        
        key = doc[DICT_ID] + "-" + doc[DICT_TERM]
        if key in cotrol_dict:
            continue
        cotrol_dict[key] = 1
                
        #doc[DICT_TUI] = lin_split[COL_UMLS_FILE_tui]
        doc[DICT_VER] = VER
        doc[DICT_SYMBOL] = False
        doc[DICT_PLURAL] = False
        
        if lin_split[COL_UMLS_FILE_str] in acr_dict:
            doc[DICT_SYMBOL] = True
        dict_coll.insert_document(doc)
    dict_coll.close()



def create_acronym_list(path, header, output_path):
    
    is_symbol_list = []
    is_symbol_list.append("ACR")
    is_symbol_list.append("AB")
    
    ofile = open(output_path, "w")
    
    for lin in file(path):
        if header:
            header = 0
            continue
        
        lin_split = lin.strip().split("\t")
        #cui    str    TUI    TTY
        if lin_split[COL_UMLS_FILE_tty] in is_symbol_list:
            ofile.write(lin)
    ofile.close()
        

def get_acronym_list(path, header):
    acr_dict = {}
    for lin in file(path):
        if header:
            header = 0
            continue
        lin_split = lin.strip().split("\t")
        acr_dict[lin_split[COL_UMLS_FILE_str]] = 1
    return acr_dict

if __name__ == '__main__':
    
    input_path = sys.argv[1]
    header = 1
    if len(sys.argv) > 2:
        header_str = sys.argv[2].lower()
        if "0" in header_str or "f" in header_str or "n" in header_str:
            header = 0
    
    blaklist_path = sys.argv[3]
    blaklist_header = 1
    header_str = sys.argv[4].lower()
    if "0" in header_str or "f" in header_str or "n" in header_str:
        blaklist_header = 0
    
    remove_processing =True
    #cuis_black_list_path = sys.path[0] + "/blackList2013.csv"
    #cuis_black_list_path = "/".join(sys.path[0].split("/")[:-2]) + "/in/blackList2013.csv"
    acr_path = "/".join(sys.path[0].split("/")[:-2]) + "/out/dictionaryDisease_acronyms.tsv"
    create_acronym_list(input_path, header, acr_path)
    
    dictionary_creation(DICT_DISEASE, input_path, header, get_acronym_list(acr_path, 0), get_cuis_black_dict(blaklist_path, blaklist_header))
    disease_normalization(DICT_DISEASE, remove_processing)