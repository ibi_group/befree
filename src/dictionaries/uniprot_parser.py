# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys, re
from datetime import datetime
from dict_constants import UNIPROT, GENE_DB_NAME, XREF_FROM_DB, ID, ENTREZ_GENE, REFSEQ,\
    GI, PDB, GO, IPI, UNIREF100, UNIREF90, UNIREF50, UNIPARC, PIR, OMIM, UNIGENE,\
    PUBMED, EMBL, EMBL_CDS, ENSEMBL, ENSEMBL_TRS, ENSEMBL_PRO, PUBMED_ADDITIONAL,\
    UNIPROTID, HISTORIC, UNIPROT_FILE_NAME_SPROT,DICT_COLL, DICT_TERM, DICT_SYMBOL,\
    VER, DICT_EC,COL_UNIPROT_NCBI_FILE_NCBI_Gene_ID, COL_UNIPROT_NCBI_FILE_Uniprot_ID
    
current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection
from progressbar import ProgressBar, Percentage, Bar

MODE_RECNAME = 2
MODE_ALTNAME = 1
MODE_CONTAINS_RECNAME = 3
MODE_CONTAINS_ALTNAME = 4
MODE_INCLUDES_RECNAME = 5
MODE_INCLUDES_ALTNAME = 6
MODE_CONTAINS = 7
MODE_INCLUDES = 8

"""
UNIPROT FIELDS - http://web.expasy.org/docs/userman.html

ID    Identification    Once; starts the entry
AC    Accession number(s)    Once or more
DT    Date    Three times
DE    Description    Once or more
GN    Gene name(s)    Optional
OS    Organism species    Once or more
OG    Organelle    Optional
OC    Organism classification    Once or more
OX    Taxonomy cross-reference    Once
OH    Organism host    Optional ----> NOT EXISTS
RN    Reference number    Once or more
RP    Reference position    Once or more
RC    Reference comment(s)    Optional
RX    Reference cross-reference(s)    Optional
RG    Reference group    Once or more (Optional if RA line)
RA    Reference authors    Once or more (Optional if RG line)
RT    Reference title    Optional
RL    Reference location    Once or more
CC    Comments or notes    Optional
DR    Database cross-references    Optional ----> NOOOOO!
PE    Protein existence    Once
KW    Keywords    Optional
FT    Feature table data    Once or more
SQ    Sequence header    Once
(blanks)    Sequence data    Once or more
//    Termination line    Once; ends the entry
"""


def store_uniprot_history(acc_num, historic_connection):
    #historic_connection = MongoConnection(GENE_DB_NAME, UNIPROT + HISTORIC + VER)
    uniprot = acc_num[0]
    for acc in acc_num[1:]:
        aux = {}
        aux[UNIPROT + ID] = uniprot
        aux[UNIPROT + HISTORIC] = acc
        historic_connection.insert_document(aux)
    #historic_connection.close()

def store_uniprot_xref(gene_file_mapping, header):
    print "Cross-reference extraction from", gene_file_mapping + "..."
    
    file_len = len(open(gene_file_mapping).readlines())
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    uniprot_connection = MongoConnection(GENE_DB_NAME, UNIPROT + VER)
    xref_connection = MongoConnection(GENE_DB_NAME, UNIPROT + XREF_FROM_DB + VER)
    xref_connection.delete_collection()
    
    # Store Uniprot cross-references
    """
    1. UniProtKB-AC
    2. UniProtKB-ID
    3. GeneID (EntrezGene)
    4. RefSeq
    5. GI
    6. PDB
    7. GO
    8. IPI
    9. UniRef100
    10. UniRef90
    11. UniRef50
    12. UniParc
    13. PIR
    14. NCBI-taxon <------- NO!
    15. MIM
    16. UniGene
    17. PubMed
    18. EMBL
    19. EMBL-CDS
    20. Ensembl
    21. Ensembl_TRS
    22. Ensembl_PRO
    23. Additional PubMed
    """
    for lin in file(gene_file_mapping):
        progvar += 1
        progress.update(progvar)
        if header:
            header = 0
            continue
        
        doc = {}
        fields = lin.strip().split("\t")
        doc[UNIPROT + ID] = fields[COL_UNIPROT_NCBI_FILE_Uniprot_ID]

        if uniprot_connection.find_one({UNIPROT + ID:doc[UNIPROT + ID]}):
            #if fields[1] != "":
            #    doc[UNIPROTID] = fields[1].replace("; ", "|")
            if fields[2] != "":
                doc[ENTREZ_GENE + ID] = fields[COL_UNIPROT_NCBI_FILE_NCBI_Gene_ID].replace("; ", "|")
            xref_connection.insert_document(doc)
    
    return xref_connection
    progress.finish()
    
   
def remove_extra_spaces(text):
    p = re.compile(r'\s+')
    return p.sub(' ', text) 

def store_uniprot_db2(gene_file_name, header):
    
    print "Terminology extraction from", gene_file_name + "..."
    
    file_len = len(open(gene_file_name).readlines())
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    uniprot_connection = MongoConnection(GENE_DB_NAME, UNIPROT + VER)
    uniprot_connection.delete_collection()
    
    historic_connection = MongoConnection(GENE_DB_NAME, UNIPROT + HISTORIC + VER)
    
    
    startswith_pat = re.compile(r'(ID|AC|DE|GN)\s\s\s')
    
    #startswith_pat = re.compile(r'(ID|AC)\s\s\s')
    #Remove:  {ECO:0000255|HAMAP-Rule:MF_03117}
    brackets_pat = re.compile(r'\s{[^{}]+}')
    
    #info = remove_extra_spaces(txt).split(" ")
    #doc["id"] = info[1]
    #doc["status"] = info[2]
    
    """    
    DE   RecName: Full=Geranylgeranyl pyrophosphate synthase;
    DE            Short=GGPP synthase;
    DE            Short=GGPPSase;
    DE            EC=2.5.1.-;
    
    DE   AltName: Full=Gamma-glutamyltransferase 1;
    DE   AltName: Full=Glutathione hydrolase 1;
    DE            EC=3.4.19.13;
    DE   AltName: Full=Leukotriene-C4 hydrolase;
    DE            EC=3.4.19.14;
    """
    
    gn_name_pat = re.compile(r'Name=([^;]+)')
    
    gn_syn_pat = re.compile(r'Synonyms=([^;]+)')
    
    for lin in file(gene_file_name):
        progvar += 1
        progress.update(progvar)
        if header:
            header = 0
            continue
        
        lin = brackets_pat.sub("",lin.strip())
        
        if startswith_pat.match(lin):
            if lin.startswith("ID   "):
                #print "---"
                info = remove_extra_spaces(lin).split(" ")
                iden = info[1]
                status = info[2][:-1]
                ac = []
                out = False
                alt_name_field = []
                alt_name = False
                alt_full = ""
                alt_short = []
                alt_ec = []
                rec_full = ""
                rec_short = []
                rec_ec = []
                gn_name = []
                gn_syn = []
                continue
                        
            elif lin.startswith("AC   "):
                ac = ac + lin.replace("AC   ", "")[:-1].split("; ")
                continue
             
            elif lin.startswith("DE   "):
                if out:
                    continue
                if lin.startswith("DE   RecName: "):
                    tag = "RecName"
                    rec_full = ""
                    rec_short = []
                    rec_ec = []
                    if "Full=" in lin:
                        rec_full = lin.split("Full=")[1][:-1]
                    if "Short=" in lin:
                        rec_short.append(lin.split("Short=")[1][:-1])
                    if "EC=" in lin:
                        rec_ec.append(lin.split("EC=")[1][:-1])
                    continue
                    
                elif lin.startswith("DE   AltName: "):
                    if alt_name:
                        aux = {}
                        if len(alt_full):
                            aux["full"] = alt_full
                        if len(alt_short):
                            aux["short"] = alt_short
                        if len(alt_ec):
                            aux["ec"] = alt_ec
                        alt_name_field.append(aux)
                    
                    alt_name = True
                    tag = "AltName"
                    alt_full = ""
                    alt_short = []
                    alt_ec = []
                    
                    if "Full=" in lin:
                        alt_full = lin.split("Full=")[1][:-1]
                    if "Short=" in lin:
                        alt_short.append(lin.split("Short=")[1][:-1])
                    if "EC=" in lin:
                        alt_ec.append(lin.split("EC=")[1][:-1])
                    continue
                    
                elif lin.startswith("DE            "):
                    if tag == "RecName":
                        if "Full=" in lin:
                            rec_full = lin.split("Full=")[1][:-1]
                        if "Short=" in lin:
                            rec_short.append(lin.split("Short=")[1][:-1])
                        if "EC=" in lin:
                            rec_ec.append(lin.split("EC=")[1][:-1])
                        
                    elif tag == "AltName":
                        if "Full=" in lin:
                            alt_full = lin.split("Full=")[1][:-1]
                        if "Short=" in lin:
                            alt_short.append(lin.split("Short=")[1][:-1])
                        if "EC=" in lin:
                            alt_ec.append(lin.split("EC=")[1][:-1])
                    continue
                
                elif lin.startswith("DE   Contains:"):
                    out = True
                elif lin.startswith("DE   Includes:"):
                    out = True
                elif "Flags:" in lin:
                    continue
                
            elif lin.startswith("GN   "):
                match = gn_name_pat.search(lin)
                if match:
                    name =  match.group(1)
                    if "{" in name:
                        name = name[:name.index("{")].strip()
                    gn_name.append(name)
                
                match = gn_syn_pat.search(lin)
                if match:
                    syn_list = match.group(1)
                    if "{" in syn_list:
                        syn_list = syn_list[:syn_list.index("{")].strip()
                    for syn in syn_list.split(", "):
                        gn_syn.append(syn)
                                
        elif lin.strip() == '//':
            # write doc
            doc = {}
            
            # id & status
            doc["id"] = iden
            doc["status"] = status
            
            # ac
            if len(ac):
                uniprot_id = ac[0]
                doc[UNIPROT + "_id"] = uniprot_id
                store_uniprot_history(ac, historic_connection)
            
            # recommended
            recommended_name = {}
            recommended_name["full"] = rec_full
            if len(rec_short):
                recommended_name["short"] = rec_short
            if len(rec_ec):
                recommended_name["ec"] = rec_ec
            if len(recommended_name.keys()):
                doc["rec_name"] = recommended_name
            
            # alternative
            if alt_name:
                aux = {}
                if len(alt_full):
                    aux["full"] = alt_full
                if len(alt_short):
                    aux["short"] = alt_short
                if len(alt_ec):
                    aux["ec"] = alt_ec
                alt_name_field.append(aux)
                
            if len(alt_name_field):
                doc["alt_name"] = alt_name_field
            
            # gene names
            gene_names = {}
            if len(gn_name):
                gene_names["name"] = gn_name
            if len(gn_syn):
                gene_names["synonyms"] = gn_syn
            
            if len(gene_names.keys()):
                doc["gene_names"] = gene_names
            
            uniprot_connection.insert_document(doc)
    
    progress.finish()
    historic_connection.close()
    return uniprot_connection
    
def get_entry(iden, term, is_simbol, is_ec):
    aux = {}
    aux[DICT_TERM] = term
    aux[UNIPROT + ID] = iden
    if is_ec:
        aux[DICT_EC] = True
        return aux
    aux[DICT_SYMBOL] = is_simbol
    return aux

def name_extraction():
    print ""
    print "Raw dictionary creation..."
    gene_connection = MongoConnection(GENE_DB_NAME, UNIPROT + VER)
    
    gene_dict_connection = MongoConnection(GENE_DB_NAME, UNIPROT + DICT_COLL + VER)
    gene_dict_connection.delete_collection()
    gene_dict_connection.create_index(UNIPROT + ID)
    
    cursor = gene_connection.find()
    file_len = cursor.count()
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    for record in cursor:
        progvar+=1
        progress.update(progvar)
        
        gene_id = record.get(UNIPROT + ID)
        
        # Recommended Name
        field = record.get("rec_name", None)
        if field:
            full = field.get("full", None)
            if full:
                gene_dict_connection.insert_document(get_entry(gene_id, full, False, False))
            short = field.get("short", None)
            if short:
                for s in short:
                    gene_dict_connection.insert_document(get_entry(gene_id, s, True, False))
            ec = field.get("ec", None)
            if ec:
                for e in ec:
                    gene_dict_connection.insert_document(get_entry(gene_id, e, False, True))
        
        # Alternative Name
        field = record.get("alt_name", None)
        if field:
            for alt_name in field:                
                full = alt_name.get("full", None)
                if full:
                    gene_dict_connection.insert_document(get_entry(gene_id, full, False, False))
                short = alt_name.get("short", None)
                if short:
                    for s in short:
                        gene_dict_connection.insert_document(get_entry(gene_id, s, True, False))
                ec = alt_name.get("ec", None)
                if ec:
                    for e in ec:
                        gene_dict_connection.insert_document(get_entry(gene_id, e, False, True))
        
        # Gene Names
        field = record.get("gene_names", None)
        if field:
            #for gn in field:
            name = field.get("name", None)
            if name:
                for n in name:
                    gene_dict_connection.insert_document(get_entry(gene_id, n, True, False))
            syn = field.get("synonyms", None)
            if syn:
                for s in syn:
                    gene_dict_connection.insert_document(get_entry(gene_id, s, True, False))
                
    progress.finish()
    print "Done!" 
    print "One collection added in \"" +GENE_DB_NAME + "\" database:"
    print "    -", UNIPROT + DICT_COLL + VER, "with", gene_dict_connection.find().count(),"entries"
    
if __name__ == '__main__':
    
    uniprot_path = sys.argv[1]
    header = 0
    header_str = sys.argv[2].lower()
    if "1" in header_str or "t" in header_str or "y" in header_str:
        header = 1
    uniprot_mapping_path  = sys.argv[3]
    
    header_map = 0
    header_str = sys.argv[4].lower()
    if "1" in header_str or "t" in header_str or "y" in header_str:
        header_map = 1
        
    print ""
    print "##################"
    print "# Uniprot Source #"
    print "##################"
    print ""
    
    
    uniprot_connection = store_uniprot_db2(uniprot_path, header)
    xref_connection = store_uniprot_xref(uniprot_mapping_path, header_map)
    
    print "Done!"
    print "Two new collections added in \"" +GENE_DB_NAME + "\" database:"
    print "    -", UNIPROT + VER, "with",uniprot_connection.find().count(), "rows"
    print "    -", UNIPROT + XREF_FROM_DB + VER, "with",xref_connection.find().count(), "rows"
    uniprot_connection.close()
    xref_connection.close()
    
    name_extraction()
    print ""
    
    