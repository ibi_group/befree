# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys
from dict_constants import ENTREZ_GENE, HGNC, GENE_DB_NAME,\
    ID, XREF_FROM_DB, VER, COL_HGNC_FILE_ID,\
    COL_HGNC_FILE_Approved_Symbol, COL_HGNC_FILE_Approved_Name,\
    COL_HGNC_FILE_Status, COL_HGNC_FILE_Previous_Symbols,\
    COL_HGNC_FILE_Previous_Name, COL_HGNC_FILE_Synonyms,\
    COL_HGNC_FILE_Name_Synonyms, COL_HGNC_FILE_Entrez_Gene_ID,\
    COL_HGNC_FILE_UniProt_ID, UNIPROT,\
    COL_HGNC_FILE_Enzyme_IDs, DICT_COLL, DICT_TERM, DICT_SYMBOL,\
    DICT_EC

current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection
from progressbar import ProgressBar, Percentage, Bar

def create_document(fields):
    doc = {}
    # HGNC ID
    field = fields[COL_HGNC_FILE_ID]
    if ":" in field:
        field = field.split(":")[1]
    doc[HGNC + "_id"] = field
    
    # Approved Symbol
    string = fields[COL_HGNC_FILE_Approved_Symbol]
    if not string == "":
        doc["approved_symbol"] = string
        
    # Approved Name
    string = fields[COL_HGNC_FILE_Approved_Name]
    if not string == "":
        doc["approved_name"] = string
        
    # Previous Symbols
    string = fields[COL_HGNC_FILE_Previous_Symbols]
    if not string == "":
        #doc["previous_symbols"] = string.split(", ")
        if "|" in string:
            string = string[1:-1]
        doc["previous_symbols"] = string.split("|")
        
        
    # Previous Name
    string = fields[COL_HGNC_FILE_Previous_Name]
    if not string == "":
        #aux = string.split('", "')
        #aux[0] = aux[0][1:]
        #aux[-1] = aux[-1][:-1]
        #doc["previous_names"] = aux
        if "|" in string:
            string = string[1:-1]
        doc["previous_names"] = string.split("|")
        
    # Synonyms
    string = fields[COL_HGNC_FILE_Synonyms]
    if not string == "":
        #doc["synonyms"] = string.split(", ")
        if "|" in string:
            string = string[1:-1]
        doc["synonyms"] = string.split("|")
        
    # Name Synonyms
    string = fields[COL_HGNC_FILE_Name_Synonyms]
    if not string == "":
        #aux = string.split('", "')
        #aux[0] = aux[0][1:]
        #aux[-1] = aux[-1][:-1]
        #doc["name_synonyms"] = aux
        if "|" in string:
            string = string[1:-1]
        doc["name_synonyms"] = string.split("|")
        
    # Enzyme IDs
    string = fields[COL_HGNC_FILE_Enzyme_IDs]
    if not string == "":
        #doc["ec"] = string.split(", ")
        if "|" in string:
            string = string[1:-1]
        doc["ec"] = string.split("|")
    return doc

def cross_references(fields):
    record = {}
    #gene_id_flag = 0
    #  HGNC ID
    field = fields[COL_HGNC_FILE_ID]
    if ":" in field:
        field = field.split(":")[1]
    record[HGNC + ID] = field
    
    # Entrez Gene ID
    string = fields[COL_HGNC_FILE_Entrez_Gene_ID]
    if not string == "":
        #gene_id_flag = 1
        #record[ENTREZ_GENE + ID] = string.replace(", ", "|")   
        if "|" in string:
            string = string[1:-1]
        record[ENTREZ_GENE + ID] = string  
    
    # Entrez Gene ID(supplied by NCBI)
    #string = fields[COL_HGNC_FILE_Entrez_Gene_ID_Supp]        
    #if string != "" and not gene_id_flag:
    #    record[ENTREZ_GENE + ID] = string.replace(", ", "|")
    
    # UniProt ID (mapped data supplied by UniProt)
    string = fields[COL_HGNC_FILE_UniProt_ID]        
    if not string == "":
        #record[UNIPROT + ID] = string.replace(", ", "|")
        if "|" in string:
            string = string[1:-1]
        record[UNIPROT + ID] = string
    return record

def store_hgnc(gene_file_name, header):
    print ""
    print "###############"
    print "# HGNC Source #"
    print "###############"
    print ""
    print "Terminology extraction from", gene_file_name + "..."
    
    file_len = len(open(gene_file_name).readlines())
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    hgnc_connection = MongoConnection(GENE_DB_NAME, HGNC + VER)
    hgnc_connection.delete_collection()
    hgnc_xref = MongoConnection(GENE_DB_NAME, HGNC + XREF_FROM_DB + VER)
    hgnc_xref.delete_collection()
    for l in file(gene_file_name):
        progvar += 1
        progress.update(progvar)
        if header:
            header = 0
            continue
        fields = l.strip().split("\t") + [""]*50
        if fields[COL_HGNC_FILE_Status].lower() != "approved":
            continue         
        # HGNC FIELDS 
        doc = create_document(fields)            
        hgnc_connection.insert_document(doc)
        # CROSS-REFERENCES 
        doc = cross_references(fields)
        if doc:
            if len(doc.items()) > 1:
                hgnc_xref.insert_document(doc)
    
    progress.finish()
    
    print "Done!"
    print "Two new collections added in \"" +GENE_DB_NAME + "\" database:"
    print "    -", HGNC + VER, "with",hgnc_connection.find().count(), "rows"
    print "    -", HGNC + XREF_FROM_DB + VER, "with",hgnc_xref.find().count(), "rows"

def get_entry(iden, term, is_simbol, is_ec):
    aux = {}
    aux[DICT_TERM] = term
    aux[HGNC + ID] = iden
    if is_ec:
        aux[DICT_EC] = True
        return aux
    aux[DICT_SYMBOL] = is_simbol
    return aux

def name_extraction():
    print ""
    print "Raw dictionary creation..."
    
    gene_connection = MongoConnection(GENE_DB_NAME, HGNC + VER)
    
    gene_dict_connection = MongoConnection(GENE_DB_NAME, HGNC + DICT_COLL + VER)
    gene_dict_connection.delete_collection()
    gene_dict_connection.create_index(HGNC + ID)
    
    cursor = gene_connection.find()
    file_len = cursor.count()
    progress = ProgressBar(widgets=[Percentage(), Bar()], maxval = file_len).start()
    progvar = 0
    
    for record in cursor:
        progvar+=1
        progress.update(progvar)
        gene_id = record.get(HGNC + ID)
        
        a_symbol = record.get("approved_symbol", None)
        if a_symbol:
            gene_dict_connection.insert_document(get_entry(gene_id, a_symbol, True, False))
                
        a_name = record.get("approved_name", None)
        if a_name:
            gene_dict_connection.insert_document(get_entry(gene_id, a_name, False, False))
        
        p_symbols = record.get("previous_symbols", None)
        if p_symbols:
            for p in p_symbols:
                gene_dict_connection.insert_document(get_entry(gene_id, p, True, False))
        
        p_names = record.get("previous_names", None)
        if p_names:
            for p in p_names:
                gene_dict_connection.insert_document(get_entry(gene_id, p, False, False))
            
        synonyms = record.get("synonyms", None)
        if synonyms:
            for p in synonyms:
                gene_dict_connection.insert_document(get_entry(gene_id, p, True, False))
        
        name_synonyms = record.get("name_synonyms", None)
        if name_synonyms:
            for p in name_synonyms:
                gene_dict_connection.insert_document(get_entry(gene_id, p, False, False))
                
        ec_codes = record.get("ec", None)
        if ec_codes:
            for ec in ec_codes:
                gene_dict_connection.insert_document(get_entry(gene_id, ec, False, True))
    
    progress.finish()   
    print "Done!" 
    print "One collection added in \"" +GENE_DB_NAME + "\" database:"
    print "    -",HGNC + DICT_COLL + VER, "with", gene_dict_connection.find().count(),"entries"
    
def main(gene_file_name, header):
    store_hgnc(gene_file_name, header)
    name_extraction()
    
if __name__ == '__main__':
    hgnc_path = sys.argv[1]
    header = 1
    if len(sys.argv) > 2:
        header_str = sys.argv[2].lower()
        if "0" in header_str or "f" in header_str or "n" in header_str:
            header = 0
    print ""
    main(hgnc_path, header)
    print ""