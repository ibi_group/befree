# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys, re, regex, inflect
from time import time
from dict_constants import DICT_DB_NAME, DICT_TERM, DICT_ID,\
    DICT_GENE, CHR, LOW, SPL, PMK, DPK, ORD,\
    STP, LEN, SET, DICT_SYMBOL, DICT_EC,\
    REP, DICT_ORIGINAL, DICT_TUI, REN,\
    DICT_VER, DICT_PLURAL, ID, ENTREZ_GENE,\
    UNIPROT, HGNC, VER

current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
from MongoConnection import MongoConnection

plural_engine = inflect.engine()

def init_dict_collection(dict_name, sufix):
    dict_conn = MongoConnection(DICT_DB_NAME, dict_name)
    new_dict_name = dict_name + sufix
    dict_new_conn = MongoConnection(DICT_DB_NAME,  new_dict_name)
    dict_new_conn.delete_collection()
    dict_new_conn.create_index(DICT_ID)
    dict_new_conn.create_index(DICT_TERM)
    dict_conn.copy_collection(dict_new_conn)    
    return dict_conn, dict_new_conn

def char_replace(term):
    term = regex.sub(ur'[àâäáÀÂÄÁ]', 'a', term)
    term = regex.sub(ur'[éèëêÈËÊÉ]', 'e', term)
    term = regex.sub(ur'[ïíìîÏÎÍÌ]', 'i', term)
    term = regex.sub(ur'[öóòôÖÔÓÒ]', 'o', term)
    term = regex.sub(ur'[ûüùúÜÛÙÚ]', 'u', term)
    term = regex.sub(ur'[çÇ]', 'c', term)
    term = regex.sub(ur'[ñÑ]', 'n', term)
    return term
    

def char_replace_step(dict_name):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, CHR)
    
    #First Step, Original
    res = dict_new_conn.find()
    for rec in res:
        rec[DICT_ORIGINAL] = rec.get(DICT_TERM)
        dict_new_conn.insert_document(rec)
    
    
    res = dict_new_conn.find({DICT_TERM:re.compile(ur'[àâäáéèëêïíìîöóòôûüùúçñÀÂÄÁÈËÊÉÏÎÍÌÖÔÓÒÜÛÙÚÇÑ]')})
    if res.count():
        for rec in res:
            rec[DICT_TERM] = char_replace(rec.get(DICT_TERM))
            dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + CHR

def removing_tuis_step(dict_name, del_tuis):
    TUI = "_" + "_".join(del_tuis)
    dict_conn, dict_new_conn = init_dict_collection(dict_name, TUI)
    
    for tui in del_tuis:
        dict_new_conn.delete_document({DICT_TUI:tui})
    
    dict_conn.close()
    dict_new_conn.close()
    
    return dict_name + TUI


def removing_entries_step(dict_name):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, REN)
    dict_new_conn.delete_document({DICT_TERM:re.compile(r'\[{1}')})
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + REN


def plural_step(dict_name):
    PLU = "_plu"
    dict_conn, dict_new_conn = init_dict_collection(dict_name, PLU)
    res = dict_new_conn.find({DICT_TERM:re.compile(r'[A-Z]')})
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            term_split = term.split(" ")
            last_word = term_split[-1]
            if not last_word.isdigit():
                if last_word[-1] == "y":
                    last_word = last_word[:-1] + "ie"
                last_word = last_word + "s"
                rec[DICT_PLURAL] = True
                term_split[-1] = last_word
                term = " ".join(term_split)
            rec[DICT_TERM] = term
            dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + PLU

def lower_step(dict_name):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, LOW)
    res = dict_new_conn.find({DICT_TERM:re.compile(r'[A-Z]')})
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            rec[DICT_TERM] = term.lower()
            dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + LOW

def char_digit_split_step(dict_name):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, SPL)
    res = dict_new_conn.find({DICT_TERM:re.compile(r'\d[A-Za-z]|[A-Za-z]\d')})
    if res.count():
        for rec in res:
            rec.pop("_id")
            term = rec.get(DICT_TERM)
            rec[DICT_TERM] = split_charDigit(term)
            dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + SPL

def split_charDigit(text):    
    dig = re.compile(r'\d') 
    char = re.compile(r'[A-Za-z]') 
    finds = []
    if len(text) > 1:
        for i in range(1, len(text)):
            if char.match(text[i-1]):
                if dig.match(text[i]):
                    finds.append(i-1)
            elif char.match(text[i]):
                if dig.match(text[i-1]):
                    finds.append(i-1)  
    inc = 0
    for i in range(0, len(finds)):
        text = text[0:finds[i]+1+inc] + ' ' + text[finds[i]+1+inc:]
        inc += 1       
    return text

def remove_punt_marks_step(dict_name):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, PMK)
    pattern = re.compile(r'[^a-zA-Z0-9\s]{1}')
    res = dict_new_conn.find({DICT_TERM:pattern})
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            term = pattern.sub(" ", term) 
            rec[DICT_TERM] = remove_extra_spaces(term).strip()
            dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + PMK

def create_new_terminology_by_replacement(dict_name, rep_dict):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, REP)
    
    rep_list = rep_dict.keys()
    
    res = dict_new_conn.find({DICT_TERM:re.compile(r''+"|".join(rep_list))})
    if res.count():
        for rec in res:
            rec.pop("_id")
            term = rec.get(DICT_TERM)
            for rep in rep_list:
                term = term.replace(rep, rep_dict.get(rep))
            rec[DICT_TERM] = term
            dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + REP

def remove_set_of_chars_step(dict_name, rep_list):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, SET)
    res = dict_new_conn.find({DICT_TERM:re.compile(r''+"|".join(rep_list))})
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            for rep in rep_list:
                term = term.replace(rep, " ")
            rec[DICT_TERM] = remove_extra_spaces(term)
            dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + SET

# Specific disease dictionary rules
def disease_punt_marks_revome_step(dict_name):
    dict_conn, dict_new_conn, log_conn = init_dict_collection(dict_name, DPK)
    res = dict_conn.find({DICT_TERM:re.compile(r'\[')})
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            ini = term.find("[")
            end = term.find("]")
            rec[DICT_TERM] = term[:ini] + term[end+1:]
            dict_new_conn.insert_document(rec)
            doc = {}
            doc["_id"] = rec.get("_id")
            log_conn.insert_document(doc)
    
    res = dict_conn.find({DICT_TERM:re.compile(r'\{')})
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            ini = term.find("{")
            end = term.find("}")
            rec[DICT_TERM] = term[:ini] + term[end+1:]
            dict_new_conn.insert_document(rec)
            doc = {}
            doc["_id"] = rec.get("_id")
            log_conn.insert_document(doc)
    
    dict_conn.close()
    log_conn.close() 
    dict_new_conn.close()
    return dict_name + DPK
    
def remove_extra_spaces(text):
    p = re.compile(r'\s+')
    return p.sub(' ', text)
           

def order_step(dict_name):
    dict_conn = MongoConnection(DICT_DB_NAME, dict_name)
    new_dict_name = dict_name + ORD
    dict_new_conn = MongoConnection(DICT_DB_NAME,  new_dict_name)
    dict_new_conn.delete_collection()
    dict_new_conn.create_index(DICT_ID)
    dict_new_conn.create_index(DICT_TERM)
    res = dict_conn.find()
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            doc = {}
            doc["_id"] = rec.get("_id")
            doc[DICT_TERM] = "".join(sorted(term.split()))
            doc[DICT_ID] = rec.get(DICT_ID)
            dict_new_conn.insert_document(doc)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + ORD

def add_elem_dictionary(dictionary, key, elem, repet = False):
    if key in dictionary:
        aux = dictionary.get(key)
        if repet:
            aux.append(elem)
            dictionary[key] = aux
        else:    
            if not elem in aux:
                aux.append(elem)
                dictionary[key] = aux
    else:
        dictionary[key] = [elem]
    return dictionary

def merge_dictionary_final(dict_name, new_dict_name):
    dict_conn = MongoConnection(DICT_DB_NAME, dict_name)
    dict_new_conn = MongoConnection(DICT_DB_NAME,  new_dict_name)
    dict_new_conn.delete_collection()
    dict_new_conn.create_index(DICT_ID)
    dict_new_conn.create_index(DICT_TERM)
    
    terms = {}
    tuis = {}
    symbols = {}
    ec = {}
    plural = {}
    records = dict_conn.find()
    for record in records:
        term = record.get(DICT_TERM).strip()
        iden = record.get(DICT_ID)
        
        add_elem_dictionary(terms, term, iden)
        
        #print record.get(DICT_SYMBOL)
        if symbols.has_key(term):
            if not symbols.get(term) and record.get(DICT_SYMBOL):
                #print term, 1
                symbols[term] = True
        else:
            if record.get(DICT_SYMBOL):
                #print term, 2
                symbols[term] = True
            else:
                #print term, 3
                symbols[term] = False
        
        if ec.has_key(term):
            if not ec.get(term) and record.get(DICT_EC):
                #print term, 1
                ec[term] = True
        else:
            if record.get(DICT_EC):
                #print term, 2
                ec[term] = True
            else:
                #print term, 3
                ec[term] = False
        
        if term in plural:
            if not plural.get(term) and record.get(DICT_PLURAL):
                #print term, 1
                plural[term] = True
        else:
            if record.get(DICT_PLURAL):
                #print term, 2
                plural[term] = True
            else:
                #print term, 3
                plural[term] = False
        
    for tup in terms.items():
        doc = {}
        doc[DICT_TERM] = tup[0]
        doc[DICT_ID] = tup[1]#"|".join(tup[1])
        doc[DICT_SYMBOL] = symbols.get(tup[0])
        doc[DICT_EC] = ec.get(tup[0])
        doc[DICT_PLURAL] = plural.get(tup[0])
        #if plural.get(tup[0]):
        #    print tup[0]
        doc[DICT_VER] = VER
        dict_new_conn.insert_document(doc)

def create_dict_final(dict_name, new_dict_name, len_num, plural_falg = False):
    # ONLY FOR DISEASE
    dict_conn = MongoConnection(DICT_DB_NAME, dict_name)
    dict_new_conn = MongoConnection(DICT_DB_NAME,  new_dict_name)
    dict_new_conn.delete_collection()
    dict_new_conn.create_index(DICT_ID)
    dict_new_conn.create_index(DICT_TERM)
    
    id_list = dict_conn.get_collection().distinct(DICT_ID)
    plural_dic = {}
    for cid in id_list:
        res = dict_conn.find({DICT_ID:cid})
        terms = {}
        terms_ec = {}
        #terms_plu = {}
        for t in res:
            new_name = remove_extra_spaces(t.get(DICT_TERM)).strip()
            #if "drug induced" in new_name:
            #    new_name = new_name.replace("drug induced", "induced")
            
            if len(new_name) > len_num:
                if terms.has_key(new_name):
                    if not terms.get(new_name) and t.get(DICT_SYMBOL):
                        terms[new_name] = True
                else:
                    if t.get(DICT_SYMBOL):
                        terms[new_name] = True
                    else:
                        terms[new_name] = False
                    
                    if t.get(DICT_EC):
                        terms_ec[new_name] = True
                    else:
                        terms_ec[new_name] = False
                #terms[new_name] = 1
        
        for key in terms.keys():
            doc = {}
            doc[DICT_ID] = cid
            doc[DICT_TERM] = key
            doc[DICT_SYMBOL] = terms[key]
            doc[DICT_EC] = terms_ec[key]
            if plural_falg:
                if not doc[DICT_SYMBOL]:
                    term_split = key.split(" ")
                    if len(key)>5:
                        last_word = term_split[-1]
                        if not last_word.isdigit():
                            last_word = plural_engine.plural(last_word)
                            term_split[-1] = last_word
                            key2 = " ".join(term_split)
                            if plural_engine.singular_noun(last_word):
                                if not key2 in terms:
                                    doc2 = {}
                                    doc2[DICT_ID] = cid
                                    doc2[DICT_TERM] = key2
                                    doc2[DICT_SYMBOL] = doc[DICT_SYMBOL]
                                    doc2[DICT_EC] = doc[DICT_EC]
                                    doc2[DICT_PLURAL] = True
                                    dict_new_conn.insert_document(doc2)
                                plural_dic[key2] = 1
                            else:
                                if not key2 in terms:
                                    doc2 = {}
                                    doc2[DICT_ID] = cid
                                    doc2[DICT_TERM] = key2
                                    doc2[DICT_SYMBOL] = doc[DICT_SYMBOL]
                                    doc2[DICT_EC] = doc[DICT_EC]
                                    doc2[DICT_PLURAL] = False
                                    dict_new_conn.insert_document(doc2)
                                
            dict_new_conn.insert_document(doc)       
    if plural_falg:        
        for key in plural_dic:
            rec = dict_new_conn.find_one({"term":key})
            rec[DICT_PLURAL] = True
            dict_new_conn.insert_document(rec)
    dict_new_conn.close()

def get_greek_letters_2_string():
    greek_symbols_lw = {}
    greek_symbols_lw[u"α"] = "alpha"
    greek_symbols_lw[u"β"] = "beta"
    greek_symbols_lw[u"γ"] = "gamma"
    greek_symbols_lw[u"δ"] = "delta"
    greek_symbols_lw[u"ε"] = "epsilon"
    greek_symbols_lw[u"ζ"] = "zeta"
    greek_symbols_lw[u"η"] = "eta"
    greek_symbols_lw[u"θ"] = "theta"
    greek_symbols_lw[u"ι"] = "iota"
    greek_symbols_lw[u"κ"] = "kappa"
    greek_symbols_lw[u"λ"] = "lambda"
    greek_symbols_lw[u"μ"] = "mu"
    greek_symbols_lw[u"ν"] = "nu"
    greek_symbols_lw[u"ξ"] = "xi"
    greek_symbols_lw[u"π"] = "pi"
    greek_symbols_lw[u"ρ"] = "rho"
    greek_symbols_lw[u"σ"] = "sigma"
    greek_symbols_lw[u"τ"] = "tau"
    greek_symbols_lw[u"υ"] = "upsilon"
    greek_symbols_lw[u"φ"] = "phi"
    greek_symbols_lw[u"χ"] = "chi"
    greek_symbols_lw[u"ψ"] = "psi"
    greek_symbols_lw[u"ω"] = "omega"
    return greek_symbols_lw

def get_string_2_greek_letters():
    greek_symbols_lw = get_greek_letters_2_string()
    new_greek_symbols_lw = {}
    for tup in greek_symbols_lw.items():
        new_greek_symbols_lw[tup[1]] = tup[0]
    return new_greek_symbols_lw

def add_greek_letters(dict_name):
    
    sym = "_alpha"
    dict_conn, dict_new_conn = init_dict_collection(dict_name, sym)
    
    greek_letters = get_string_2_greek_letters()
    
    
    reg_ex = r"\b(" + '|'.join(greek_letters.keys()) + r")\b"
    #reg_ex = r'|'.join(greek_letters.keys())
    #reg_ex = r"[^a-z|\b](" + '|'.join(greek_letters.keys()) + r")[^a-z|\b]"
    
    #res = dict_new_conn.find({DICT_TERM:re.compile(r'|'.join(greek_letters.keys()))})
    res = dict_new_conn.find({DICT_TERM:re.compile(reg_ex)})
    
    #greek_pattern = regex.compile(r'|'.join(greek_letters.keys()))
    greek_pattern = regex.compile(reg_ex)
    
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            
            matches = greek_pattern.finditer(term)
            new_term = term
            
            for m in matches:
                greek_char = m.group(1)
                
                new_term_dict = {}
                new_term_dict[remove_extra_spaces(new_term.replace(greek_char, greek_letters.get(greek_char))).strip()] = 1
                new_term_dict[remove_extra_spaces(new_term.replace(" " + greek_char, greek_letters.get(greek_char))).strip()] = 1
                new_term_dict[remove_extra_spaces(new_term.replace(greek_char + " ", greek_letters.get(greek_char))).strip()] = 1
                new_term_dict[remove_extra_spaces(new_term.replace(" " + greek_char + " ", greek_letters.get(greek_char))).strip()] = 1
                new_term_dict[remove_extra_spaces(new_term.replace(greek_char, " " + greek_letters.get(greek_char))).strip()] = 1
                new_term_dict[remove_extra_spaces(new_term.replace(greek_char, greek_letters.get(greek_char) + " ")).strip()] = 1
                new_term_dict[remove_extra_spaces(new_term.replace(greek_char, " " + greek_letters.get(greek_char) + " ")).strip()] = 1
                
                for k in new_term_dict.keys():
                    rec.pop("_id")
                    rec[DICT_TERM] = k
                    dict_new_conn.insert_document(rec)

    dict_conn.close()
    dict_new_conn.close()
    return dict_name + sym

def add_greek_letters_old(dict_name):
    
    sym = "_alpha"
    dict_conn, dict_new_conn = init_dict_collection(dict_name, sym)
    
    greek_letters = get_string_2_greek_letters()
    
    #reg_ex = r"\b(" + '|'.join(greek_letters.keys()) + r")\b"
    reg_ex = r'|'.join(greek_letters.keys())
    
    #res = dict_new_conn.find({DICT_TERM:re.compile(r'|'.join(greek_letters.keys()))})
    res = dict_new_conn.find({DICT_TERM:re.compile(reg_ex)})
    
    #greek_pattern = regex.compile(r'|'.join(greek_letters.keys()))
    greek_pattern = regex.compile(reg_ex)
    
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            
            matches = greek_pattern.finditer(term)
            new_term = term
            new_term_2 = term
            for m in matches:
                ini = m.start()
                end = m.end()
                
                new_term = new_term.replace(term[ini:end], greek_letters.get(term[ini:end]))
                    
                if ini > 0:
                    
                    rec.pop("_id")
                    rec[DICT_TERM] = new_term
                    dict_new_conn.insert_document(rec)
                    
                    if term[ini-1] == " ":
                        new_term_2 = new_term_2.replace(" " + term[ini:end], greek_letters.get(term[ini:end]))
                    else:
                        new_term_2 = new_term_2.replace(term[ini:end], " " + greek_letters.get(term[ini:end]))
                        
                    rec[DICT_TERM] = new_term_2
                    dict_new_conn.insert_document(rec)
                    
                    #new pattern
                    new_term_list = []
                    new_term_list.append(new_term_2.replace(" " + term[ini:end], greek_letters.get(term[ini:end])))
                    new_term_list.append(new_term_2.replace(term[ini:end] + " ", greek_letters.get(term[ini:end])))
                    new_term_list.append(new_term_2.replace(" " + term[ini:end] + " ", greek_letters.get(term[ini:end])))
                    new_term_list.append(new_term_2.replace(term[ini:end], " " + greek_letters.get(term[ini:end])))
                    new_term_list.append(new_term_2.replace(term[ini:end], greek_letters.get(term[ini:end]) + " "))
                    new_term_list.append(new_term_2.replace(term[ini:end], " " + greek_letters.get(term[ini:end]) + " "))
                    
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + sym

def gene_normalization(dict_ori_name):
    collections = ""
    """
    if len(dbs):
        collections = "_"
        if ENTREZ_GENE in dbs:
            collections = collections + "N"
        if UNIPROT in dbs:
            collections = collections + "U"    
        if HGNC in dbs:
            collections = collections + "H"
    """
    coll_name = dict_ori_name + collections
    dict_name = dict_ori_name + collections + VER 
    
    print coll_name.upper() + " CREATION..."
    
    delete_collections = []

    print "Char replace"
    dict_name = char_replace_step(dict_name)
    delete_collections.append(dict_name)
    
    dict_name = remove_terms_len_step(dict_name, 1)
    delete_collections.append(dict_name)
    
    dict_name = remove_terms_len_step(dict_name, 2)
    delete_collections.append(dict_name)
    
    print "Lower Step"
    dict_name = lower_step(dict_name)
    delete_collections.append(dict_name)
    
    print "Char-Digit split"
    dict_name = char_digit_split_step(dict_name)
    delete_collections.append(dict_name)
    
    print "Roman numbers"
    roman_path = sys.path[0] + "/roman_numbers_500.list" 
    dict_name = transform_roman_numbers_step(dict_name, roman_path)
    delete_collections.append(dict_name)
    
    print "Word Swap"
    rep_dict = {}
    rep_dict["tumor"] = "tumour" 
    dict_name = create_new_terminology_by_replacement(dict_name, rep_dict)
    delete_collections.append(dict_name)
    
    print "Removing character"
    remove_list = ["'s","`s"]
    dict_name = remove_set_of_chars_step(dict_name, remove_list)
    delete_collections.append(dict_name)
    
    print "Removing Punt Marks"
    dict_name = remove_punt_marks_step(dict_name)
    delete_collections.append(dict_name)
    
    print "Add Greek Letters"
    dict_name = add_greek_letters(dict_name)
    delete_collections.append(dict_name)
    
    print "Final dictionary Creation"
    create_dict_final(dict_name, coll_name + "_final"+ VER, 1)
    
    print "Merge Final dictionary"
    merge_dictionary_final(coll_name + "_final"+ VER, coll_name + "_final_merged"+ VER)
    
    print "Removing additional collections"
    for coll in delete_collections:
        dict_new_conn = MongoConnection(DICT_DB_NAME,  coll)
        dict_new_conn.delete_collection()

def remove_terms_len_step(dict_name, chars_len):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, LEN + str(chars_len))
    pat = re.compile(r'^[^\s]{'+str(chars_len)+'}$')
    res = dict_new_conn.find({"term":pat})
    for r in res:
        dict_new_conn.delete_document({ID:r.get(ID)}) 
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + LEN + str(chars_len)
    
def remove_stopwords(dict_name):
    dict_conn, dict_new_conn = init_dict_collection(dict_name, STP)
    stoplist = []
    
    stopwords_path = sys.path[0] + "/stopwords" 
    
    for word in file(stopwords_path):
        stoplist.append(word.strip())
    res = dict_conn.find()
    
    if res.count():
        for rec in res:
            term = rec.get(DICT_TERM)
            if not term in stoplist:
                dict_new_conn.insert_document(rec)
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + STP

def disease_normalization(dict_ori_name, remove_processing):
    
    coll_name = dict_ori_name
    dict_name = dict_ori_name + VER 
    
    print coll_name.upper() + " CREATION..."
    
    delete_collections = []
    
    print "Char replace"
    dict_name = char_replace_step(dict_name)
    delete_collections.append(dict_name)
    
    print "Lowercase"
    dict_name = lower_step(dict_name)
    delete_collections.append(dict_name)
    
    print "Char-Digit split"
    dict_name = char_digit_split_step(dict_name)
    delete_collections.append(dict_name)
    
    print "Remove chars"
    dict_name = remove_set_of_chars_step(dict_name, ["'s","`s", "<sup>", "</sup>", "&#124;", "[x]", "[d]", "[m]", "[disease/finding]"])
    delete_collections.append(dict_name)
    
    print "Remove punt marks"
    dict_name = remove_punt_marks_step(dict_name)
    delete_collections.append(dict_name)
    
    print "Add Greek Letters"
    dict_name = add_greek_letters(dict_name)
    delete_collections.append(dict_name)
    
    print "Final dictionary Creation"
    create_dict_final(dict_name, coll_name + "_final" + VER , 1)
    
    print "Merge Final dictionary"
    merge_dictionary_final(coll_name + "_final" + VER, coll_name + "_final_merged" + VER)
    
    if remove_processing:
        print "Removing auxiliar collections"
        for coll in delete_collections:
            dict_new_conn = MongoConnection(DICT_DB_NAME,  coll)
            dict_new_conn.delete_collection()
      

def transform_roman_numbers_step(dict_name, roman_path):
    sym = "_rom"
    pattern = regex.compile(r'\b(\d+)\b')
    dict_conn, dict_new_conn = init_dict_collection(dict_name, sym)
    res = dict_new_conn.find({DICT_TERM:re.compile(r'\b(\d+)\b')})
    roman_dict = {}
    
    i=-1
    final_num = 30
    for lin in file(roman_path):
        i+=1
        if not i:
            continue
        lin_split = lin.strip().split("\t")
        arabic = lin_split[0]
        roman = lin_split[1]
        roman_dict[arabic]=roman.lower()
        if i==final_num:
            break
        
    if res.count():
        for rec in res:
            if not rec.get(DICT_EC):
                rec.pop("_id")
                term = rec.get(DICT_TERM)
                rec[DICT_TERM] = transform_roman_numbers(term, pattern, roman_dict)
                dict_new_conn.insert_document(rec)
            
    dict_conn.close()
    dict_new_conn.close()
    return dict_name + sym

def transform_roman_numbers(text, pattern, roman_dict):
    match = pattern.search(text)
    text_before = ""
    while(match):
        ini = match.start()
        end = match.end()
        roman = roman_dict.get(match.group())
        if roman:
            text = text[:ini] + roman + text[end:]
        else:
            text_before = text_before + text[:end]
            text = text[end:]
        match = pattern.search(text)
    return text_before + text