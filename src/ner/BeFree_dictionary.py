# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys, regex
from BeFree_utils import term_curation
from BeFree_constants import GENE_ENTITY, DISEASE_ENTITY, DICT_DISEASE_NAME,\
    TERM_INFO_TOTAL, TERM_INFO_CONCEPT_ID, TERM_INFO_SYMBOL,DICT_GENE_NAME, \
    TERM_INFO_EC, TERM_INFO_PLURAL,ACRONYM_INFO_CONCEPT_ID, \
    ACRONYM_INFO_ORIGIN, ACRONYM_INFO_PATTERN, ACRONYM_INFO_TOTAL

current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)
dictionaries_path = "/".join(current_path.split("/")[:-1]) + "/dictionaries"
sys.path.append(dictionaries_path)

from MongoConnection import MongoConnection
    
from dict_constants import DICT_TERM, DICT_EC, DICT_SYMBOL, DICT_PLURAL, DICT_ID,\
    DICT_DB_NAME
    
from disease_dictionary import get_cuis_black_dict


class EntityDictionary(object):
    
    def __init__(self):
        self.pattern = None
        self.type = None
        self.terms_dict = {}
        self.parent = None
        self.blacklist = {}
        self.stopwords = {}
         
    def get_mongo_connection(self, entity_type):
        if entity_type == GENE_ENTITY:
            return MongoConnection(DICT_DB_NAME, DICT_GENE_NAME)
        elif entity_type == DISEASE_ENTITY:
            return MongoConnection(DICT_DB_NAME, DICT_DISEASE_NAME)
        
    def pattern_creation(self, entity_type, synonyms_blacklist, ids_list, term_alt_dict = []):
        self.type = entity_type
        current_path = sys.path[0]
        stopwords_file = "/".join(current_path.split("/")[:-1]) + "/dictionaries/stopwords_list1_lextek" 
        self.stopwords = {}
        for lin in file(stopwords_file):
            lin = lin.strip()
            self.stopwords[lin.lower()] = 1
        
        dict_conn = self.get_mongo_connection(entity_type)
        spc = u""
        if len(ids_list):
            records = dict_conn.find({DICT_ID:{"$in":ids_list}})
        else:
            records = dict_conn.find()
        
        pattern_dict = {}
        for record in records:
            term = record.get(DICT_TERM)
            term_info_value = [None]*TERM_INFO_TOTAL
            if not term in self.stopwords:

                term_info_value[TERM_INFO_CONCEPT_ID] = record.get(DICT_ID)
                term_info_value[TERM_INFO_SYMBOL] = record.get(DICT_SYMBOL)
                term_info_value[TERM_INFO_EC] = record.get(DICT_EC)
                
                # aBravo 12.06.2015 
                term_info_value[TERM_INFO_PLURAL] = record.get(DICT_PLURAL)
                # aBravo 12.06.2015 - END
                
                self.terms_dict[term + spc] = term_info_value
                pattern_dict[term + " "] = 1
        
        if len(term_alt_dict):
            for record in term_alt_dict:
                term_info_value = [None]*TERM_INFO_TOTAL
                term = record.get(DICT_TERM)
                term_info_value[TERM_INFO_CONCEPT_ID] = record.get(DICT_ID)
                term_info_value[TERM_INFO_SYMBOL] = record.get(DICT_SYMBOL)
                term_info_value[TERM_INFO_EC] = record.get(DICT_EC, False)
                term_info_value[TERM_INFO_PLURAL] = record.get(DICT_PLURAL, False)
                self.terms_dict[term + spc] = term_info_value
                pattern_dict[term + " "] = 1
                
        self.pattern = regex.compile(ur"\b\L<options>", options=pattern_dict.keys(), flags=regex.V1)
        self.blacklist = {}
        
        for term in synonyms_blacklist:
            self.blacklist[term_curation(term.strip())] = 1
        
        
    def is_in_blacklist(self, term):
        return term in self.blacklist
    
    def is_in_stopwords(self, term):
        return term in self.blacklist
        
    def get_blacklist(self):
        return self.blacklist
    
    def get_conceptID(self, term_norm):
        if not term_norm in self.terms_dict:
            return []
        return self.terms_dict[term_norm][TERM_INFO_CONCEPT_ID]
    
    def get_symbol(self, term_norm):
        return self.terms_dict[term_norm][TERM_INFO_SYMBOL]
    
    def get_plural(self, term_norm):
        return self.terms_dict[term_norm][TERM_INFO_PLURAL]
    
    def get_ec(self, term_norm):
        return self.terms_dict[term_norm][TERM_INFO_EC]
    
    def has_term(self, term_norm):
        return self.terms_dict.get(term_norm)
    
    def get_type(self):
        return self.type


class AcronymDictionary(object):
    def __init__(self):
        self.terms_dict = {}
        
    def get_conceptID(self, term):
        if not term in self.terms_dict:
            return []
        return self.terms_dict[term][ACRONYM_INFO_CONCEPT_ID]
    
    def get_origin_id(self, term):
        return self.terms_dict[term][ACRONYM_INFO_ORIGIN]
    
    def get_pattern(self, term):
        return self.terms_dict[term][ACRONYM_INFO_PATTERN]
    
    def get_terms(self):
        return self.terms_dict
    
    def get_pattern_list(self):
        patt_list = []
        for term in self.terms_dict:
            patt_list.append(self.terms_dict[term][ACRONYM_INFO_PATTERN])
        return patt_list
    
    def has_term(self, term):
        return self.terms_dict.get(term)
    
    def set_conceptID(self, term, conceptID):
        self.terms_dict[term][ACRONYM_INFO_CONCEPT_ID] = conceptID
    
    def set_origin_id(self, term, iden):
        self.terms_dict[term][ACRONYM_INFO_ORIGIN] = iden
    
    def set_pattern(self, term, pat):
        if not term in self.terms_dict:
            self.terms_dict[term] = [None]*ACRONYM_INFO_TOTAL
        self.terms_dict[term][ACRONYM_INFO_PATTERN] = pat