# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import regex
from BeFree_result import DocumentResult, SentenceResult, EntityResult
from BeFree_constants import GENE_ENTITY, DISEASE_ENTITY, DISEASE_PATTERN_LIST,\
    GENE_PATTERN_REGEX, GENE_PATTERN_LIST, DISEASE_PATTERN_REGEX,\
    CELL_LINE_PATTERN_LIST, CELL_LINE_PATTERN, GENE_FAMILY_PATTERN_LIST,\
    GENE_FAMILY_PATTERN_REGEX, DNA_PATTERN
from BeFree_utils import get_units_pattern, get_percent_pattern,\
    get_real_number_pattern, replace_xml_tags_filtering, percent_filtering,\
    units_of_measurement_filtering, real_numbers_filtering, special_characters,\
    get_normalization_term, get_extra_spaces_position, remove_extra_spaces,\
    get_results_pattern, get_greek_letters_lw, get_offset_correction,\
    term_curation, enumerations_detection
from BeFree_dictionary import AcronymDictionary, EntityDictionary

class BeFreeNER(object):
    def __init__(self, entity_type, synonyms_blacklist = [], ids_list = [], term_alt_dict = []):
        #INI 19.04.2016 
        self.UNITS_PATTERN = get_units_pattern()
        self.PERCENT_PATTERN = get_percent_pattern()
        self.REAL_NUMBER_PATTERN = get_real_number_pattern()
        self.RESULTS_PATTERN = get_results_pattern()
        #END 19.04.2016 
        self.document_result = DocumentResult()
        self.entity_dictionary = EntityDictionary()
        self.acronym_dictionary = AcronymDictionary()
        self.sentence = ""
        self.sentence_trans = ""
        self.offset_corr = {}
        self.currentSentenceResult = SentenceResult()
        self.dictionary_creation(entity_type, synonyms_blacklist, ids_list, term_alt_dict)
    #INI 19.04.2016 
    def get_units_pattern(self):
        return self.UNITS_PATTERN
    
    def get_percent_pattern(self):
        return self.UNITS_PATTERN
    
    def get_real_number_pattern(self):
        return self.REAL_NUMBER_PATTERN
    #END 19.04.2016
     
    def has_results(self):
        return self.document_result.has_results()
    
    def get_entity_dictionary(self):
        return self.entity_dictionary
    
    def entity_filtering(self, sent_num):
        sent_id = self.document_result.get_id() + "-" + str(sent_num)
        sent_res = self.document_result.sentence_results_dict.get(sent_id)
        
        if not sent_res:
            return
        
        entities_list = sent_res.get_sentence_entities_sorted()
        sent_text = sent_res.get_sentence()
        for entity in entities_list:
            if entity.get_mention() != sent_text[entity.get_ini():entity.get_end()]:
                self.document_result.remove_entity_by_id(entity.get_id())
                continue
    
    def get_document_result(self):
        return self.document_result
    
    def write_result(self):
        return self.document_result.to_string()
    
    def get_entity_concepts(self):
        concepts = {}
        for entity in self.currentSentenceResult.sentence_entities_sorted:
            for concept_id in entity.get_concepts():
                concepts[concept_id] = 1
        return concepts
    
    def get_entities_sorted(self):
        return self.currentSentenceResult.sentence_entities_sorted
    
    def acronym_extraction_filtering(self, entity_term_norm):
        if entity_term_norm.replace(" ", "").isnumeric():
            return False
        elif len(entity_term_norm.replace(" ", "")) == 1:
            return False
        elif entity_term_norm in ["ii", "iii", "iv", "vi", "vii", "viii", "ix", "xi", "xii", "xiii", "xiv", "xv"]:
            return False
        elif regex.match(ur'0x\s?\d+', entity_term_norm):
            return False
        elif regex.match(ur'rs\d+', entity_term_norm):
            return False
        return True
    
    def dictionary_creation(self, entity_type, synonyms_blacklist, ids_list = [], term_alt_dict = []):
        self.entity_dictionary = EntityDictionary()
        self.entity_dictionary.pattern_creation(entity_type, synonyms_blacklist, ids_list, term_alt_dict)
    
    def entity_recognition(self, document_info):
        self.acronym_dictionary = AcronymDictionary()
        self.sentence = ""
        self.sentence_trans = ""
        self.offset_corr = {}
        self.currentSentenceResult = SentenceResult()
        self.document_result = DocumentResult()
        self.document_result.set_id(document_info.get_id())
        self.document_result.set_issn(document_info.get_issn())
        self.document_result.set_journal(document_info.get_journal_abbrev())
        self.document_result.set_num_sections(len(document_info.label_abstract_dict))
        self.document_result.set_structured(False)
        self.document_result.set_year(document_info.get_year())
        self.document_result.set_mesh_list(document_info.get_mesh_list())
        self.document_result.set_num_sentences(document_info.get_num_sentences())
        sent_num = -1
        for section_num in range(0,len(document_info.key_list)):
            section = document_info.label_abstract_dict.get(section_num)
            sentences = document_info.abstract_sent_list.get(section)
            for sentence in sentences:
                sent_num +=1
                self.currentSentenceResult = SentenceResult()
                self.currentSentenceResult.set_id(self.document_result.get_id() + "-" + str(sent_num))
                self.currentSentenceResult.set_section(section)
                self.currentSentenceResult.set_section_num(section_num)
                self.currentSentenceResult.set_sent_num(sent_num)
                #INI 19.04.2016
                #self.sentence = u"" + replace_xml_tags_filtering(sentence)
                #self.sentence_trans, self.offset_corr = sentence_prefiltering(sentence)
                self.sentence_preprocessing(sentence)
                #END 19.04.2016
                
                self.currentSentenceResult.set_sentence(self.sentence)
                self.pattern_recognition()

    
    def sentence_preprocessing(self, sentence):
        sentence = u""+sentence
        sentence = replace_xml_tags_filtering(sentence)#.decode('utf-8')
        self.sentence = sentence
        sentence = percent_filtering(sentence, self.get_percent_pattern())
        sentence = units_of_measurement_filtering(sentence, self.get_units_pattern())
        sentence = real_numbers_filtering(sentence, self.get_real_number_pattern())
        sentence = enumerations_detection(sentence)
        sentence = special_characters(sentence)
        sentence = get_normalization_term(sentence)
        self.offset_corr = get_extra_spaces_position(sentence)
        self.sentence_trans = remove_extra_spaces(sentence) + u" X"
    
    def entity_recognition_from_text(self, text):
        self.acronym_dictionary = AcronymDictionary()
        self.currentSentenceResult = SentenceResult()
        self.document_result = DocumentResult()
        #INI 19.04.2016
        #self.sentence = u"" + replace_xml_tags_filtering(text)
        #self.sentence_trans, self.offset_corr = sentence_prefiltering(text)
        self.sentence_preprocessing(text)
        #END 19.04.2016
        self.currentSentenceResult.set_sentence(self.sentence)
        self.pattern_recognition()
    
    def mention_filtering(self, ent_res):
        entity_type = ent_res.get_type()
        detected_norm_term = ent_res.get_norm_mention()
        detected_original_term = ent_res.get_mention()
        symbol = self.entity_dictionary.get_symbol(detected_norm_term)
        ec = self.entity_dictionary.get_ec(detected_norm_term)
        features = ent_res.get_features()
        sentence = self.currentSentenceResult.get_sentence()
        ini = ent_res.get_ini()
        end = ent_res.get_end()
        
        if entity_type == GENE_ENTITY:
            if not ec:
                
                if "CLINE" in features:
                    return False
                
                sym_list = []
                sym_list.append(":")
                sym_list.append("=")
                sym_list.append(";")
                sym_list.append("%")
                
                if detected_original_term == "ncRNA":
                    return False
                if detected_norm_term == "ca2" or detected_norm_term == "ca 2":
                    return False
                #NEW FILTERING 
                if regex.match(r'or\s?[0-9]', detected_norm_term):
                    return False
                
                if regex.match(r'are\s?[0-9]', detected_norm_term):
                    return False
                
                if detected_original_term == "COLD" and "PCR" in sentence[end:end+5]:
                    return False
                
                if self.entity_dictionary.is_in_stopwords(detected_original_term):
                    return False
                
                if detected_norm_term in self.entity_dictionary.get_blacklist() and not "GENE" in features:# and not "EXTRACTED" in features:
                        return False
                
                if detected_norm_term == "insulin":
                    return False
                #NEW FILTERING 
                
                if regex.search(r''+"|".join(sym_list), detected_original_term) or (len(detected_original_term)>=20 and detected_original_term.find("; (")>=0):
                    return False
                
                term_test = detected_norm_term.replace(" ", "")
                if term_test.isdigit():
                    return False
                
                if symbol:
                    sym_list.append(",")
                    detected_original_term_split = detected_original_term.split(" ")
                    if len(detected_original_term_split) == 2:
                        if detected_original_term_split[0].isdigit() and detected_original_term_split[1].islower():
                            return False
                        if detected_original_term_split[1].isdigit() and detected_original_term_split[0].islower():
                            return False
                    
                    if regex.search(r''+"|".join(sym_list), detected_original_term):
                        return False
                    if regex.findall(r'\b[p|P][0-9]{1,5}\b', detected_original_term):
                        #ini = ent_res.get_ini()
                        #end = ent_res.get_end()
                        #sentence = self.currentSentenceResult.get_sentence()
                        if ini > 0:
                            ini-=1
                        if end > len(sentence):
                            end+=1
                        if sentence[ini] == " ":
                            if end >= len(sentence)-1: # Change 13.10.2014 if end == len(sentence)-1:
                                return True
                            elif sentence[end] == " ":
                                return True
                                
                        return False
                    
                        #return True
                    if regex.match(r'[a|o]\s?\d+', detected_norm_term):
                        return False
                    if detected_norm_term in self.entity_dictionary.get_blacklist() and not "GENE" in features:# and not "EXTRACTED" in features:
                        return False
                    if len(term_test) < 3 and not "GENE" in features and not "EXTRACTED" in features:
                        return False
                    if len(term_test) == 3:
                        if regex.search(r'[0-9]', term_test):
                            if not "GENE" in features and not "EXTRACTED" in features and not "il" in term_test:
                                return False
                        if regex.search(r'[a-z]', detected_original_term):
                            if not "GENE" in features and not "EXTRACTED" in features:
                                return False
                        # aBravo 24122014
                        
                    if detected_original_term.isupper():
                        return True
                    
                    new_term = regex.sub('|'.join(get_greek_letters_lw().keys()), "A", detected_original_term)
                    if new_term.isupper():
                        return True
                    
                    if regex.findall(r'[0-9]', detected_original_term):
                        return True
                    
                    if len(detected_original_term) > 4:
                        if sum(1 for c in detected_original_term if c.isupper()) >= len(detected_original_term)/2:
                            return True
                else:
                    if detected_norm_term in self.entity_dictionary.get_blacklist():
                        return False
                    return True
            if "GENE" in features:
                return True
            
            return False
        elif entity_type == DISEASE_ENTITY:
            
            if regex.match(r'\d+ or \d+', detected_norm_term):
                return False
            
            if regex.match(r'grade \d+', detected_norm_term):
                return False
            
            if regex.match(r'seizures in \d+ patient', detected_norm_term):
                return False
            
            if "DISEASE" in features:
                if detected_norm_term in DISEASE_PATTERN_LIST:
                    if not "cancer" in detected_norm_term and not "tumo" in detected_norm_term:
                        return False
                #if "DT" in features and not "DN" in features and not "DA" in features:
                #    return False
                return True
            if "DNA" in features:
                return False
            sym_list = []
            sym_list.append(":")
            sym_list.append("=")
            sym_list.append(";")
            sym_list.append("%")
            term_test = detected_norm_term.replace(" ", "")
            if len(term_test) < 3:
                return False
            if regex.search(r''+"|".join(sym_list), detected_original_term):# or (len(detected_original_term)>=20 and detected_original_term.find("; (")>=0):
                    return False
            if symbol:
                if detected_original_term.isupper():
                    if detected_norm_term in self.entity_dictionary.get_blacklist():
                        return False
                    return True
                else:
                    return False
            else:
                if len(detected_norm_term) < 5:
                    if detected_original_term.isupper():
                        return True
                    else:
                        if detected_norm_term == "pain":
                            return True
                        return False
                else:
                    if detected_norm_term in self.entity_dictionary.get_blacklist():
                        return False
                    return True
        else:
            return True


    def pattern_recognition(self):
        entity_type = self.entity_dictionary.get_type()
        matches = self.entity_dictionary.pattern.finditer(self.sentence_trans, pos=None, endpos=None, overlapped=True, concurrent=True) 
        
        for m in matches:
            ent_res = EntityResult()
            ini = m.start()
            end = m.end()-1
            if end-ini == 0:
                continue
            detected_norm_term = self.sentence_trans[ini:end]
            ini, end = get_offset_correction(self.offset_corr, ini, end)
            detected_original_term = self.sentence[ini:end]
            
            ent_res.set_ini(ini)
            ent_res.set_end(end)
            ent_res.set_mention(detected_original_term)
            ent_res.set_norm_mention(detected_norm_term)
            ent_res.set_type(self.entity_dictionary.get_type())
            ent_res.set_concepts(self.entity_dictionary.get_conceptID(detected_norm_term))
            ent_res.set_id(self.currentSentenceResult.get_id() + "-" + str(ini) + "-" + str(end))
            ent_res.set_type(entity_type)
            
            #if self.mention_filtering(ent_res): Cambio! lo pongo mas abajo 
            feature = "LONGTERM"
            if self.entity_dictionary.get_symbol(ent_res.get_norm_mention()):
                feature = "SYMBOL"
            if ent_res.get_type() == DISEASE_ENTITY and len(ent_res.get_norm_mention()) < 5 and ent_res.get_mention().isupper():
                feature = "SYMBOL"
                
            ent_res.add_feature(feature)
            
            # aBravo 15.06.2015
            # PLURAL MANAGEMENT
            if ent_res.has_feature("SYMBOL"):
                if self.sentence[end:end+1] == "s":
                    feature = "PLURAL"
                    ent_res.add_feature(feature)
                    end += 1
                    #detected_original_term = self.sentence[ini:end]
            elif self.entity_dictionary.get_plural(ent_res.get_norm_mention()):
                feature = "PLURAL"
                ent_res.add_feature(feature)
            

            if ini > 0 and end < len(self.sentence) and self.sentence[ini-1] == "(" and self.sentence[end] == ")":
                feature = "()"
                ent_res.add_feature(feature)
            
            new_end = end
            if GENE_PATTERN_REGEX.match(self.sentence[end+1:]):
                new_end = end+1+GENE_PATTERN_REGEX.match(self.sentence[end+1:]).end()
                ent_res.add_feature("GENE")
                ent_res.add_feature("GN")
            if detected_norm_term.split(" ")[-1] in GENE_PATTERN_LIST:
                ent_res.add_feature("GENE")
                ent_res.add_feature("GT")
                
            
            if DISEASE_PATTERN_REGEX.match(self.sentence[end+1:]):
                new_end = end+1+DISEASE_PATTERN_REGEX.match(self.sentence[end+1:]).end()
                ent_res.add_feature("DISEASE")
                ent_res.add_feature("DN")
            if detected_norm_term.split(" ")[-1] in DISEASE_PATTERN_LIST:
                ent_res.add_feature("DISEASE")
                ent_res.add_feature("DT")
            
            
            if CELL_LINE_PATTERN.match(self.sentence[end+1:]):
                new_end = end+1+CELL_LINE_PATTERN.match(self.sentence[end+1:]).end()
                ent_res.add_feature("CLINE")
                ent_res.add_feature("CN")
            if detected_norm_term.split(" ")[-1] in CELL_LINE_PATTERN_LIST:
                ent_res.add_feature("CLINE")
                ent_res.add_feature("CT")
                
            
            if GENE_FAMILY_PATTERN_REGEX.match(self.sentence[end+1:]):
                new_end = end+1+GENE_FAMILY_PATTERN_REGEX.match(self.sentence[end+1:]).end()
                ent_res.add_feature("GFAM")
                ent_res.add_feature("FN")
            if detected_norm_term.split(" ")[-1] in GENE_FAMILY_PATTERN_LIST:
                ent_res.add_feature("GFAM")
                ent_res.add_feature("FT")
                
            
            if DNA_PATTERN.match(self.sentence[end+1:]):
                new_end = end+1+DNA_PATTERN.match(self.sentence[end+1:]).end()
                ent_res.add_feature("DNA")
                ent_res.add_feature("NN")
                
            match2 = regex.match(ur'[\s]{,2}[1-9][s]?', self.sentence[end+1:])
            if match2:
                new_end = end+1+match2.end()
                ent_res.add_feature("NUMBER")
                
            match2 = regex.match(ur'[\s]{,2}['+"".join(get_greek_letters_lw().keys())+'][s]?', self.sentence[end+1:])
            if match2:
                new_end = end+1+match2.end()
                ent_res.add_feature("ALPHA")
            
            ent_res.add_feature("DICTIONARY")
            
            if not self.mention_filtering(ent_res):
                continue   
                      
            # ACRONYM EXTRACTION: LONG_TERM (ACR)
            new_term_symbol = ""
            last_word_from_detected_original_term = ent_res.get_mention().split(" ")[-1]
            match = regex.match(ur'\([a-zA-Z0-9\-]+', last_word_from_detected_original_term)
            
            if match:
                new_term_symbol = last_word_from_detected_original_term[1:]
                if regex.search(r'[A-Z]', new_term_symbol):
                    div_mention = " ".join(ent_res.get_mention().split(" ")[:-1])
                    div_end = ent_res.get_end() - len(last_word_from_detected_original_term) - 1
                    
                    ent_res.set_end(div_end)
                    ent_res.set_mention(div_mention)
                    ent_res.set_norm_mention(term_curation(div_mention))
                    ent_res.set_id(self.currentSentenceResult.get_id() + "-" + str(ent_res.get_ini()) + "-" + str(div_end))
                    
                    # PLURAL MANAGEMENT
                    if self.sentence[end-1:end] == "s":
                        new_term_symbol = self.sentence[ini:end-1]
                else:
                    new_term_symbol = ""
            
            if not len(new_term_symbol):
                match = regex.match( ur'\(([a-zA-Z0-9\(\)\-'+"".join(get_greek_letters_lw().keys())+']+)\)', self.sentence[new_end+1:])
                if match:
                    end = new_end
                    ini = end+2+match.start()
                    end = end+match.end()
                    new_term_symbol = self.sentence[ini:end]
                    
                    
                    # aBravo 15.06.2015
                    # PLURAL MANAGEMENT
                    if self.sentence[end-1:end] == "s":
                        new_term_symbol = self.sentence[ini:end-1]
 
                    if GENE_PATTERN_REGEX.match(self.sentence[end+1:]):
                        ent_res.add_feature("GENE")
                        ent_res.add_feature("GA")
                    
                    if DISEASE_PATTERN_REGEX.match(self.sentence[end+1:]):
                        ent_res.add_feature("DISEASE")
                        ent_res.add_feature("DA")
                    
                    if CELL_LINE_PATTERN.match(self.sentence[end+1:]):
                        ent_res.add_feature("CLINE")
                        ent_res.add_feature("CA")
                    
                    if DNA_PATTERN.match(self.sentence[end+1:]):
                        ent_res.add_feature("DNA")
                        ent_res.add_feature("NA")
                    
                    if GENE_FAMILY_PATTERN_REGEX.match(self.sentence[end+1:]):
                        ent_res.add_feature("GFAM")
                        ent_res.add_feature("FA")
            
            if len(new_term_symbol):
                if not self.acronym_dictionary.has_term(new_term_symbol):
                    pat = regex.compile(ur'[^a-bA-Z0-9](' + new_term_symbol.replace("]","\]").replace("[","\[").replace("(","\(").replace(")","\)").replace(".","\.") + ur')[s]?[^a-zA-Z0-9]')
                    self.acronym_dictionary.set_pattern(new_term_symbol, pat)
                    self.acronym_dictionary.set_conceptID(new_term_symbol, ent_res.get_concepts())
                    self.acronym_dictionary.set_origin_id(new_term_symbol, ent_res.get_id())
            
            self.document_result.add_sentence_result(self.currentSentenceResult)        
            self.document_result.add_entity_result(ent_res)
                    
        # ACRONYM EXTRACTION: LONG_TERM (ACR)
        pattern_acr_list = self.acronym_dictionary.get_pattern_list()
        
        for pat in pattern_acr_list:
            matches = pat.finditer(self.sentence, pos=None, endpos=None, overlapped=True, concurrent=True)
            for m in matches:
                ini = m.start()+1
                end = m.end()-1
                # aBravo 15.06.2015
                #new_term_symbol = self.sentence[ini:end]
                new_term_symbol = m.group(1)
                is_plural = False
                if self.sentence[end-1:end] == "s":
                    is_plural = True
                    end -=1
                
                
                idn = self.currentSentenceResult.get_id() + "-" + str(ini) + "-" + str(end)
                original_id = self.acronym_dictionary.get_origin_id(new_term_symbol)
                
                if self.document_result.has_entity_result(idn):
                    self.document_result.get_entity_result(idn).add_feature("EXTRACTED")
                    self.document_result.get_entity_result(idn).set_origin_id(original_id)
                    
                    # aBravo 15.06.2015
                    # PLURAL MANAGEMENT
                    if is_plural:
                        self.document_result.get_entity_result(idn).add_feature("PLURAL")
                        end += 1
                    

                    if GENE_PATTERN_REGEX.match(self.sentence[end+1:]):
                        self.document_result.get_entity_result(idn).add_feature("GENE")
                        self.document_result.get_entity_result(idn).add_feature("GN")
                        
                    if DISEASE_PATTERN_REGEX.match(self.sentence[end+1:]):
                        self.document_result.get_entity_result(idn).add_feature("DISEASE")
                        self.document_result.get_entity_result(idn).add_feature("DN")
                    
                    if CELL_LINE_PATTERN.match(self.sentence[end+1:]):
                        self.document_result.get_entity_result(idn).add_feature("CLINE")
                        self.document_result.get_entity_result(idn).add_feature("CN")
                        
                    if DNA_PATTERN.match(self.sentence[end+1:]):
                        self.document_result.get_entity_result(idn).add_feature("DNA")
                        self.document_result.get_entity_result(idn).add_feature("NN")

                else:
                    new_term_symbol_norm = term_curation(new_term_symbol)
                    if self.acronym_extraction_filtering(new_term_symbol_norm):
                        acr_res = EntityResult()
                        acr_res.set_type(entity_type)
                        acr_res.set_ini(ini)
                        acr_res.set_end(end)
                        acr_res.set_mention(new_term_symbol)
                        acr_res.set_norm_mention(new_term_symbol_norm)
                        acr_res.set_type(self.entity_dictionary.get_type())
                        #original_id = self.acronym_dictionary.get_origin_id(new_term_symbol)
                        acr_res.set_origin_id(original_id)
                        concepts_id = self.acronym_dictionary.get_conceptID(new_term_symbol)
                        acr_res.set_concepts(concepts_id)
                        acr_res.set_id(idn)
                        acr_res.add_feature("SYMBOL")
                        acr_res.add_feature("EXTRACTED")
                        
                        # aBravo 15.06.2015
                        # PLURAL MANAGEMENT
                        if is_plural:
                            feature = "PLURAL"
                            acr_res.add_feature(feature)
                            end += 1

                        if self.sentence[ini-1] == "(" and self.sentence[end] == ")":
                            acr_res.add_feature("()")
                        
                        if GENE_PATTERN_REGEX.match(self.sentence[end+1:]):
                            acr_res.add_feature("GENE")
                            acr_res.add_feature("GN")
                            
                        if DISEASE_PATTERN_REGEX.match(self.sentence[end+1:]):
                            acr_res.add_feature("DISEASE")
                            acr_res.add_feature("DN")
                        
                        if CELL_LINE_PATTERN.match(self.sentence[end+1:]):
                            acr_res.add_feature("CLINE")
                            acr_res.add_feature("CN")
                        
                        if DNA_PATTERN.match(self.sentence[end+1:]):
                            acr_res.add_feature("DNA")
                            acr_res.add_feature("NN")
                            
                        self.document_result.add_sentence_result(self.currentSentenceResult)    
                        self.document_result.add_entity_result(acr_res) 
                        
        if self.document_result.get_sentence_result(self.currentSentenceResult.get_id()):      
            self.document_result.get_sentence_result(self.currentSentenceResult.get_id()).overlap_entity_filtering()
            self.entity_filtering(self.currentSentenceResult.get_sent_num())
            