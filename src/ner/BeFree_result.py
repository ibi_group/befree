# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import regex
from BeFree_utils import add_elem_dictionary

class DocumentResult(object):
    def __init__(self):
        self.id = "NA"
        self.year = "NA"
        self.journal = "NA"
        self.issn = "NA"
        self.structured = False
        self.num_sections = 0
        self.num_sentences = 0
        self.sentence_results_dict = {}
        self.mesh_list = []
    
    def get_mesh_list(self):
        return self.mesh_list
    
    def set_mesh_list(self, mlst):
        self.mesh_list = mlst
    
    def get_num_sentences(self):
        return self.num_sentences
    
    def set_num_sentences(self, ns):
        self.num_sentences = ns
    
    def remove_entity_by_id(self, ent_id):
        sent_id = "-".join(ent_id.split("-")[0:2])
        sent_res = self.sentence_results_dict[sent_id]
        sent_res.remove_entity_by_id(ent_id)
        if not sent_res.has_results():
            self.sentence_results_dict.pop(sent_id)
            
    def get_entities_in_sentence(self, num_sent):
        sent_id = self.id + "-" + str(num_sent)
        sent_res = self.sentence_results_dict.get(sent_id)
        if not sent_res:
            return []
        return sent_res.get_sentence_entities_sorted()
        
    def has_sentence(self, sent_id):
        return sent_id in self.sentence_results_dict
    
    def has_results(self):
        return len(self.sentence_results_dict)
    
    def get_entity_result(self, idn):
        sent_id = "-".join(idn.split("-")[:2])
        return self.sentence_results_dict[sent_id].get_entity_result(idn)
    
    def add_entity_result(self, entr):
        ent_id = entr.get_id()
        sent_id = "-".join(ent_id.split("-")[:2])
        self.sentence_results_dict[sent_id].add_entity_result(entr)
        
    def add_sentence_result(self, sentr):
        if not sentr.get_id() in self.sentence_results_dict:
            self.sentence_results_dict[sentr.get_id()] = sentr
    
    def update_sentence_result(self, sentr):
        self.sentence_results_dict[sentr.get_id()] = sentr
    
    def has_entity_result(self, idn):
        ent_id = idn
        sent_id = "-".join(ent_id.split("-")[:2])
        if not sent_id in self.sentence_results_dict:
            return False
        return self.sentence_results_dict[sent_id].get_entity_result(idn)
    
    def get_sentence_result(self, idn):
        return self.sentence_results_dict.get(idn)

    def set_id(self, did):
        self.id = did
        
    def get_id(self):
        return self.id

    def set_year(self, year):
        self.year = year
        
    def get_year(self):
        return self.year

    def set_journal(self, journal):
        self.journal = journal
        
    def get_journal(self):
        return self.journal
    
    def set_issn(self, issn):
        self.issn = issn
        
    def get_issn(self):
        return self.issn
    
    def set_structured(self, structured):
        self.structured = structured
    
    def set_num_sections(self, num_sections):
        self.num_sections = num_sections
        
    def get_num_sections(self):
        return self.num_sections
    
    def to_string(self):
        lines = []
        doc_info = []
        doc_info.append(self.id)
        doc_info.append(self.year)
        doc_info.append(self.journal)
        doc_info.append(self.issn)
        
        sent_sorted = {}
        for sent_id in self.sentence_results_dict:
            sent_sorted = add_elem_dictionary(sent_sorted, self.get_sentence_result(sent_id).get_sent_num(), sent_id)
        
        for num_sent in sorted(sent_sorted):
            key_list = sent_sorted[num_sent]
        
            for sent_id in key_list:
                sent = self.sentence_results_dict[sent_id]
                sent_info = []
                sent_info.append(sent.section)
                sent_info.append(str(sent.section_num))
                sent_info.append(str(sent.sent_num))
                
                for ent in sent.sentence_entities_sorted:
                    ent_info = []
                    ent_info.append("|".join(map(str,ent.concepts)))
                    ent_info.append("|".join(ent.features))
                    ent_info.append(ent.norm_mention)
                    ent_info.append(ent.mention)
                    ent_info.append(str(ent.ini) + "#" + str(ent.end))
                    ent_info.append(ent.origin)
                    new_line = doc_info + sent_info + ent_info + [sent.sentence]
                    lines.append("\t".join(new_line).encode('utf-8'))
        res = ""
        if len(lines):
            res = "\n".join(lines)
        return res

class SentenceResult(object):

    def __init__(self):
        self.id = "-"
        self.section = "-"
        self.section_num = -1
        self.sent_num = -1
        self.ini = -1
        self.end = -1
        self.sentence = "-"
        self.sentence_entities_dict = {}
        self.sentence_entities_sorted = []
        
    def remove_entity_by_id(self, ent_id):
        self.sentence_entities_dict.pop(ent_id)
        for i in range(0, len(self.sentence_entities_sorted)):
            ent= self.sentence_entities_sorted[i]
            if ent_id == ent.get_id():
                break
        del self.sentence_entities_sorted[i]
        
    def get_sentence_entities_sorted(self):
        return self.sentence_entities_sorted
    
    def has_results(self):
        return len(self.sentence_entities_dict)
    
    def add_entity_result(self, new_entr):
        if not new_entr.get_id() in self.sentence_entities_dict:
            self.sentence_entities_dict[new_entr.get_id()] = new_entr
    
    def sort_entities(self):
        if len(self.sentence_entities_dict) > 1:
            sorted_entities_dict = {}
            for ent_old in self.sentence_entities_dict.values():
                ini = ent_old.get_ini() 
                sorted_entities_dict = add_elem_dictionary(sorted_entities_dict, ini, ent_old.get_id())
            
            self.sentence_entities_sorted = []
            
            for ini in sorted(sorted_entities_dict.keys()):
                key_list = sorted_entities_dict[ini]
                for key in key_list:
                    self.sentence_entities_sorted.append(self.get_entity_result(key))
    
    def overlap_entity_filtering(self):
        if len(self.sentence_entities_dict) > 1:
            self.sort_entities()
            new_list = []
            del_ent = []
            for ent1 in self.sentence_entities_sorted:
                if len(new_list):
                    i = 0
                    for ent2 in new_list:
                        if ent1.is_overlap(ent2):
                            idn, ent_lst = ent1.overlap_selection(ent2)
                            
                            if idn == 2:
                                #INI 19.04.2016
                                new_list[i] = ent_lst[0]
                                new_list.append(ent_lst[1])
                                
                                # Merge
                                """
                                Intento de merge pero no
                                ini1 = ent_lst[0].get_ini()
                                ini2 = ent_lst[1].get_ini()
                                end1 = ent_lst[0].get_end()
                                end2 = ent_lst[1].get_end()

                                ini = ini1
                                if ini1 > ini2:
                                    ini = ini2
                                
                                end = end1
                                if end1 < end2:
                                    end = end2
                                
                                new_ent = EntityResult()
                                new_ent.set_ini(ini)
                                new_ent.set_end(end)
                                
                                del_ent.append(ent1.get_id())
                                del_ent.append(ent2.get_id())
                                """
                                #END 19.04.2016
                            
                            elif idn == ent1.get_id():
                                new_list[i] = ent1
                                del_ent.append(ent2.get_id())
                            else:
                                del_ent.append(ent1.get_id())
                            break
                        i += 1
                        if i == len(new_list):
                            new_list.append(ent1)
                            break
                else:
                    new_list.append(ent1)
            
            for idn in del_ent:
                self.sentence_entities_dict.pop(idn)
            
            self.sentence_entities_sorted = new_list
        else:
            self.sentence_entities_sorted = self.sentence_entities_dict.values()
            
    def get_entity_result(self, idn):
        return self.sentence_entities_dict.get(idn)

    def set_id(self, did):
        self.id = did
        
    def get_id(self):
        return self.id

    def set_sentence(self, sentence):
        self.sentence = sentence
        
    def get_sentence(self):
        return self.sentence
    
    def set_section_num(self, section_num):
        self.section_num = section_num
        
    def get_section_num(self):
        return self.section_num
    
    def set_sent_num(self, sent_num):
        self.sent_num = sent_num
        
    def get_sent_num(self):
        return self.sent_num
    
    def set_section(self, section):
        self.section = section
        
    def get_section(self):
        return self.section
    
    def set_ini(self, ini):
        self.ini = ini
        
    def get_ini(self):
        return self.ini
    
    def set_end(self, end):
        self.end = end
        
    def get_end(self):
        return self.end


class EntityResult(object):

    def __init__(self):
        self.id = ""
        self.type = ""
        self.mention = ""
        self.norm_mention = ""
        self.ini = 0
        self.end = 0
        self.features = []
        self.id = []
        self.concepts = []
        self.origin = "NA"
    
    def is_overlap(self, ent_res):
        ini1 = self.get_ini()
        end1 = self.get_end()
        ini2 = ent_res.get_ini()
        end2 = ent_res.get_end()
        result = True
        if (ini2 < ini1 and end2 < ini1) or (ini2 > end1 and end2 > end1):
            result = False
        return result
    
    def has_the_same_offset(self, ent_res):
        return self.id == ent_res.get_id()
        
    def overlap_selection_with_features(self, ent_res):
        ini1 = self.get_ini()
        end1 = self.get_end()
        ini2 = ent_res.get_ini()
        end2 = ent_res.get_end()
        
        return1 = self.get_id()
        return2 = ent_res.get_id()
        
        comma_patt = regex.compile(r'[,\(\)/\.]')
        if self.has_feature("EXTRACTED") and not ent_res.has_feature("EXTRACTED"):
            return return1, []
        if not self.has_feature("EXTRACTED") and ent_res.has_feature("EXTRACTED"):
            return return2, []
        if self.has_feature("LONGTERM") and not ent_res.has_feature("LONGTERM"):
            return return1, []
        if not self.has_feature("LONGTERM") and ent_res.has_feature("LONGTERM"):
            return return2, []
        if self.has_feature("LONGTERM") and ent_res.has_feature("LONGTERM"):
            if comma_patt.search(self.get_mention()) and not comma_patt.search(ent_res.get_mention()):
                return  return2, []
            if not comma_patt.search(self.get_mention()) and comma_patt.search(ent_res.get_mention()):
                return  return1, []
            if "," in self.get_mention() and not "," in ent_res.get_mention():
                return  return2, []
            if not "," in self.get_mention() and "," in ent_res.get_mention():
                return  return1, []
            #INI 19.04.2016
            if "&" in self.get_mention() and not "&" in ent_res.get_mention():
                return  return2, []
            if not "&" in self.get_mention() and "&" in ent_res.get_mention():
                return  return1, []
            #END 19.04.2016
            if ini1 < ini2 and (self.has_feature("DISEASE") or self.has_feature("GENE")):
                return return1, []
            if ini1 > ini2 and (ent_res.has_feature("DISEASE") or ent_res.has_feature("GENE")):
                return return2, []
        if self.has_feature("SYMBOL") and ent_res.has_feature("SYMBOL"):
            if self.has_feature("DICTIONARY") and not ent_res.has_feature("DICTIONARY"):
                return return1, []
            if not self.has_feature("DICTIONARY") and ent_res.has_feature("DICTIONARY"):
                return return2, []
            if ini1 < ini2 and (self.has_feature("DISEASE") or self.has_feature("GENE")):
                return return1, []
            if ini1 > ini2 and (ent_res.has_feature("DISEASE") or ent_res.has_feature("GENE")):
                return return2, []
            if self.has_feature("DISEASE") or self.has_feature("GENE"):
                if not ent_res.has_feature("DISEASE") and not ent_res.has_feature("GENE"):
                    return return1, []
            if ent_res.has_feature("DISEASE") or ent_res.has_feature("GENE"):
                if not self.has_feature("DISEASE") and not self.has_feature("GENE"):
                    return return2, []
        
        if self.get_norm_mention().split(" ")[-1] == "is" and ini2 < ini1:
            return return1, []
        if ent_res.get_norm_mention().split(" ")[-1] == "is" and ini1 < ini2:
            return return2, []
        
        #INI 19.04.2016
        if "," in self.get_mention() and not "," in ent_res.get_mention():
            return  return2, []
        if not "," in self.get_mention() and "," in ent_res.get_mention():
            return  return1, []
        
        if "&" in self.get_mention() and not "&" in ent_res.get_mention():
                return  return2, []
        if not "&" in self.get_mention() and "&" in ent_res.get_mention():
            return  return1, []
        #END 19.04.2016
        
         
        #print self.get_id().split("-")[0], self.get_id().split("-")[1]
        #print ":".join(self.get_id().split("-")[-2:]), self.get_mention().encode('utf-8'),",".join( self.get_features()),",".join(map(str,self.get_concepts()))
        #print ":".join(ent_res.get_id().split("-")[-2:]), ent_res.get_mention().encode('utf-8'),  ",".join(ent_res.get_features()),",".join(map(str,ent_res.get_concepts()))
        #print ""
        return 2, [self, ent_res]
        
    
    def overlap_selection(self, ent_res):
        
        ini1 = self.get_ini()
        end1 = self.get_end()
        ini2 = ent_res.get_ini()
        end2 = ent_res.get_end()
        
        inis = ini1 - ini2
        ends = end1 - end2
        
        return1 = self.get_id()
        return2 = ent_res.get_id()
        
        if inis > 0:
            if ends > 0:
                return self.overlap_selection_with_features(ent_res)
            elif ends <= 0:
                return return2, []
        elif inis < 0:
            if ends >= 0:
                return return1, []
            elif ends < 0:
                return self.overlap_selection_with_features(ent_res)
        else:
            if ends >= 0:
                return return1, []
            elif ends < 0:
                return return2, []
    
    
    
    def set_origin_id(self, did):
        self.origin = did
    
    def get_origin_id(self):
        return self.origin

    def set_id(self, did):
        self.id = did
        
    def get_id(self):
        return self.id

    def set_type(self, etype):
        self.type = etype
        
    def get_type(self):
        return self.type

    def set_mention(self, mention):
        self.mention = mention
        
    def get_mention(self):
        return self.mention
    
    def set_norm_mention(self, norm_mention):
        self.norm_mention = norm_mention
        
    def get_norm_mention(self):
        return self.norm_mention
    
    def set_ini(self, ini):
        self.ini = ini
        
    def get_ini(self):
        return self.ini
    
    def set_end(self, end):
        self.end = end
        
    def get_end(self):
        return self.end
    
    def add_feature(self, feature):
        if not feature in self.features:
            self.features.append(feature)
            
    def has_feature(self, feature):
        return feature in self.features
        
    def get_features(self):
        return self.features
    
    def add_concept(self, eid):
        if not eid in self.concepts:
            self.concepts.append(eid)
    
    def set_concepts(self, eid_lst):
        self.concepts = eid_lst
     
    def get_concepts(self):
        return self.concepts
