# coding: utf-8
# encoding: utf-8

"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

from BeFree_constants import MESH_FIELD, ID_FIELD, JOURNAL_ABBRE_FIELD,\
    ISSN_FIELD, YEAR_FIELD,LABEL_LIST_FIELD, ABSTRACT_LIST_FIELD,\
    ABSTRACT_LIST_BY_SENT_FIELD
from BeFree_utils import split_sentences

class DocumentInfo(object):
    

    def __init__(self, record):
        self.id = record.get(ID_FIELD, "-")
        self.sentences = []
        self.num_sentences = 0
        self.record = record
        self.key_list = {}
        self.label_abstract_dict = {}
        self.abstract_sent_list = []
        self.structured = False
        self.mesh_list = record.get(MESH_FIELD, [])
        #INI 19.04.2016
        self.issn = record.get(ISSN_FIELD, "-")
        self.journal_abbrev = record.get(JOURNAL_ABBRE_FIELD, "-")
        self.year = record.get(YEAR_FIELD, "-")
        #END 19.04.2016
        self.abstract_management(3, record)
    
    
    #INI 19.04.2016
    def get_issn(self):
        return self.issn
    def get_journal_abbrev(self):
        return self.journal_abbrev
    def get_year(self):
        return self.year
    #END 19.04.2016
    
    def get_mesh_list(self):
        return self.mesh_list
    
    def get_id(self):
        return self.id
    
    def get_record(self):
        return self.record
    
    def get_num_sentences(self):
        return self.num_sentences
    
    def get_structured(self):
        return self.structured
    
    def get_sentences(self):
        return self.sentences
    
    def abstract_management_alternative(self, parts, tokenizer, record):
            
        label = u"ALL_TEXT"
    
        label_abstract = record.get("section_list")
        abstract_text_list = record.get("section_text_list")
        
        #change
        ladict = {}
        ladict[u"TITLE"] = 0
        ladict[u"ALL_TEXT"] = 1 
        label_abstract = ladict
        #change
        
        self.label_abstract_dict = {}
        for tup in label_abstract.items():
            self.label_abstract_dict[tup[1]] = tup[0]
        self.key_list = sorted(self.label_abstract_dict.keys())
        self.abstract_sent_list = {}
        self.abstract_sent_list[self.label_abstract_dict.get(0)] = [abstract_text_list.get(self.label_abstract_dict.get(0))]
        self.sentences = [abstract_text_list.get(self.label_abstract_dict.get(0))]
        
        #if label_abstract.has_key(label): CHANGE!
        if True:
            
            
            # If abstracts has not sections
            #text = abstract_text_list.get(label) CHANGE!
            text = record.get("abstract_text").split("</Abstract>")[0]
            
            text = text.replace(").(", ")#(").replace(").[", ")#[").replace("al.(", "al#(").replace("?'", "'?").replace('?"', '"?')
            
            sentences_pre = tokenizer.tokenize(text)
            
            #aBravo 11.06.2015
            match_no_sep = [")", ";", "]", ":"]
            sentences = []
            title = 1
            for sent in sentences_pre:
                if not title:
                    #match = re.match(r'[a-z\);]', sent)   
                    match = re.match(r'[a-z\);\]:]', sent) 
                    if match:
                        sent_aux = sentences.pop()
                        sep = " "
                        #if match.group(0) == ")" or match.group(0) == ";" :
                        if match.group(0) in match_no_sep:
                            sep = ""
                        sent = sent_aux + sep + sent
                title = 0
                sentences.append(sent.replace(")#(", ").(").replace(")#[", ").[").replace("al#(","al.(").replace("'?", "?'").replace('"?','?"'))
            #aBravo 11.06.2015 - END
            
            self.sentences = self.sentences + sentences
            total_sent = len(sentences)
            if total_sent>=parts:
                div = total_sent/parts
                j = 0
                while j < parts:
                    new_label = label + "_" + str(j+1) + "/" + str(parts)
                    self.label_abstract_dict[j+1] = new_label
                    ini = j*div
                    end = j*div + div
                    if j == parts-1:
                        extra = total_sent - parts*div 
                        end += extra
                    self.abstract_sent_list[new_label] = sentences[ini:end]
                    j+=1
                self.key_list = sorted(self.label_abstract_dict.keys())
            else:
                self.abstract_sent_list[label] = sentences
        else:
            for j in range(1,len(self.key_list)):
                label = self.label_abstract_dict.get(j)
                text = abstract_text_list.get(label)
                sentences_pre = tokenizer.tokenize(text)
                
                #aBravo 11.06.2015
                sentences_aux = []
                title = 1
                for sent in sentences_pre:
                    if not title:
                        match = re.match(r'[a-z\);]', sent)   
                        if match:
                            sent_aux = sentences_aux.pop()
                            sep = " "
                            if match.group(0) == ")" or match.group(0) == ";" :
                                sep = ""
                            sent = sent_aux + sep + sent
                    title = 0
                    sentences_aux.append(sent)
                    
                sentences = []
                for sent in sentences_aux:
                    chat1 = sent[0].upper()
                    sent = chat1 + sent[1:]
                    sentences.append(sent)
                #aBravo 11.06.2015 - END
                
                self.abstract_sent_list[label] = sentences
                self.sentences = self.sentences + sentences
        self.num_sentences = len(self.sentences)
    
    def abstract_management(self, parts, record):
            
        label = u"ALL_TEXT"
        label_abstract = record.get(LABEL_LIST_FIELD)
        
        # aBravo Change 06.02.17
        #abstract_text_list = record.get(ABSTRACT_LIST_FIELD)
        abstract_text_list = record.get(ABSTRACT_LIST_BY_SENT_FIELD)
        # aBravo
        
        self.label_abstract_dict = {}
        for tup in label_abstract.items():
            self.label_abstract_dict[tup[1]] = tup[0]
        self.key_list = sorted(self.label_abstract_dict.keys())
        self.abstract_sent_list = {}
        self.abstract_sent_list[self.label_abstract_dict.get(0)] = [abstract_text_list.get(self.label_abstract_dict.get(0))]
        self.sentences = [abstract_text_list.get(self.label_abstract_dict.get(0))]
        
        if label_abstract.has_key(label):
            # If abstracts has not sections
            
            # aBravo Change 06.02.17
            #text = abstract_text_list.get(label).split("</Abstract>")[0]
            #sentences = split_sentences(text, tokenizer)
            sentences = abstract_text_list.get(label)
            # aBravo
            
            
            
            self.sentences = self.sentences + sentences
            total_sent = len(sentences)
            if total_sent>=parts:
                div = total_sent/parts
                j = 0
                while j < parts:
                    new_label = label + "_" + str(j+1) + "/" + str(parts)
                    self.label_abstract_dict[j+1] = new_label
                    ini = j*div
                    end = j*div + div
                    if j == parts-1:
                        extra = total_sent - parts*div 
                        end += extra
                    self.abstract_sent_list[new_label] = sentences[ini:end]
                    j+=1
                self.key_list = sorted(self.label_abstract_dict.keys())
            else:
                self.abstract_sent_list[label] = sentences
        else:
            for j in range(1,len(self.key_list)):
                label = self.label_abstract_dict.get(j)
                
                # aBravo Change 06.02.17
                #text = abstract_text_list.get(label).split("</Abstract>")[0]
                #sentences = split_sentences(text, tokenizer)
                sentences = abstract_text_list.get(label)
                # aBravo
                
                
                self.abstract_sent_list[label] = sentences
                self.sentences = self.sentences + sentences
        self.num_sentences = len(self.sentences)