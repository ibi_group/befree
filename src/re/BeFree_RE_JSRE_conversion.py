# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys, re


current_path = sys.path[0]
mongodb_path = "/".join(current_path.split("/")[:-1]) + "/mongodb"
sys.path.append(mongodb_path)

dict_path = "/".join(current_path.split("/")[:-1]) + "/dictionaries"
sys.path.append(dict_path)

medline_path = "/".join(current_path.split("/")[:-1]) + "/ner"
sys.path.append(medline_path)

medline_path = "/".join(current_path.split("/")[:-1]) + "/medline"
sys.path.append(medline_path)

from MongoConnection import MongoConnection

from BeFree_constants import DISEASE_ENTITY, GENE_ENTITY, RESULT_ENTITY_TYPE, RESULT_DOCID, \
    RESULT_SENT_TEXT, RESULT_ENTITY_NORM, \
    RESULT_SENT_NUM, RESULT_ENTITY_OFFSET, RESULT_ENTITY_PARENT, RESULT_ENTITY_TEXT,\
    RESULT_ENTITY_ID, RESULT_YEAR, RESULT_JOURNAL, RESULT_ISSN, RESULT_SECTION,\
    RESULT_SECTION_NUM, RESULT_COOC_DOCID, RESULT_COOC_ENTITY1_ID,\
    RESULT_COOC_ENTITY2_ID, RESULT_COOC_SECTION_NUM, RESULT_COOC_SENT_TEXT,\
    RESULT_COOC_ENTITY1_OFFSET, RESULT_COOC_ENTITY2_OFFSET,\
    RESULT_COOC_ENTITY1_TEXT, RESULT_COOC_ENTITY2_TEXT, RESULT_COOC_SENT_NUM,\
    GENE_PATTERN_REGEX, DISEASE_PATTERN_REGEX, RESULT_COOC_SECTION,\
    DICT_DISEASE_NAME, RESULT_COOC_ENTITY1_OFFSET_LIST, RESULT_COOC_ENTITY2_OFFSET_LIST,\
    RESULT_COOC_ENTITY1_NORM, RESULT_COOC_ENTITY2_NORM
    

from medline_constants import MESH_FIELD
from time import time
from dict_constants import DICT_ID, DICT_DB_NAME

from BeFree_utils import add_elem_dictionary, get_entity_information, is_overlap, overlap,\
    term_curation, get_mesh_disease_dictionary, get_re_process, get_results_screen

def simple_coocurrence_selection(gene_path, gene_header, disease_path, dis_header, output_path):
    
    disease_dict = {}
    
    for lin in file(disease_path):
        if dis_header:
            dis_header = 0
            continue
        
        lin_split = lin.strip().split("\t")
        
        pmid = lin_split[RESULT_DOCID]
        num_sent = lin_split[RESULT_SENT_NUM]
        
        key = pmid + "-" + num_sent
        disease_dict = add_elem_dictionary(disease_dict, key, lin_split, True)
    
    gene_dict = {}
    for lin in file(gene_path):
        if dis_header:
            dis_header = 0
            continue
        lin_split = lin.strip().split("\t")
        pmid = lin_split[RESULT_DOCID]
        num_sent = lin_split[RESULT_SENT_NUM]

        key = pmid + "-" + num_sent
        gene_dict = add_elem_dictionary(gene_dict, key, lin_split, True)
    
    ofile = open(output_path, "w")   
    
    for key in gene_dict:
        if key in disease_dict:
            gene_lines = gene_dict[key]
            disease_lines = disease_dict[key]
            
            for g_l in gene_lines:
                for d_l in disease_lines:
                    
                    new_line = []
                    new_line.append(g_l[RESULT_DOCID])
                    new_line.append(g_l[RESULT_YEAR])
                    new_line.append(g_l[RESULT_JOURNAL])
                    new_line.append(g_l[RESULT_ISSN])
                    new_line.append(g_l[RESULT_SECTION])
                    new_line.append(g_l[RESULT_SECTION_NUM])
                    new_line.append(g_l[RESULT_SENT_NUM])
                    new_line.append(g_l[RESULT_ENTITY_ID])
                    new_line.append(g_l[RESULT_ENTITY_TYPE])
                    new_line.append(g_l[RESULT_ENTITY_NORM])
                    new_line.append(g_l[RESULT_ENTITY_TEXT])
                    new_line.append(g_l[RESULT_ENTITY_OFFSET])
                    new_line.append(g_l[RESULT_ENTITY_PARENT])
                    new_line.append(d_l[RESULT_ENTITY_ID])
                    new_line.append(d_l[RESULT_ENTITY_TYPE])
                    new_line.append(d_l[RESULT_ENTITY_NORM])
                    new_line.append(d_l[RESULT_ENTITY_TEXT])
                    new_line.append(d_l[RESULT_ENTITY_OFFSET])
                    new_line.append(d_l[RESULT_ENTITY_PARENT])
                    new_line.append(d_l[RESULT_SENT_TEXT])
                    ofile.write("\t".join(new_line) + "\n")
                    ofile.flush()
    
    ofile.close() 

def cooc_add_all_offsets_by_sentence(input_path, output_path):
    
    #11106199    2001    Eur. Respir. J.    0903-1936    TITLE    0    0    65797    LONGTERM|NUMBER|DICTIONARY    tumour necrosis factor alpha    tumour necrosis factor alpha    49#77    NA    C0004096    LONGTERM|DICTIONARY    asthma    asthma    34#40    NA    Lack of association between adult asthma and the tumour necrosis factor alpha-308 polymorphism gene.
    #assoc_dict = {}
    dis_offsets_dict = {}
    gene_offsets_dict = {}
    for lin in file(input_path):
        lin_split = lin.strip().split("\t")
        
        #ent1_id = lin_split[RESULT_COOC_ENTITY2_ID]
        #ent2_id =lin_split[RESULT_COOC_ENTITY1_ID]
        pmid = lin_split[RESULT_COOC_DOCID]
        num_sent = lin_split[RESULT_COOC_SENT_NUM]
        
        #assoc_id = "-".join([ent1_id, gene_id, pmid, num_sent])
        
        #add_elem_dictionary(assoc_dict, assoc_id, lin_split)
        
        sent_id = "-".join([pmid, num_sent])
        offset1 = lin_split[RESULT_COOC_ENTITY1_OFFSET]
        offset2 = lin_split[RESULT_COOC_ENTITY2_OFFSET]
        
        add_elem_dictionary(gene_offsets_dict, sent_id, offset1)
        add_elem_dictionary(dis_offsets_dict, sent_id, offset2)
        
    ofile = open(output_path, "w")
    
    for lin in file(input_path):
        lin_split = lin.strip().split("\t")
        
        pmid = lin_split[RESULT_COOC_DOCID]
        num_sent = lin_split[RESULT_COOC_SENT_NUM]
        
        sent_id = "-".join([pmid, num_sent])
        
        lin_split.append("|".join(gene_offsets_dict[sent_id]))
        lin_split.append("|".join(dis_offsets_dict[sent_id]))
        
        ofile.write("\t".join(lin_split)+"\n")
    ofile.close()   

def cooc_creation_2_jsre(input_path, output_path, ent1_A, ent1_B, ent2_A, ent2_B):
    
    #20967760    2011    Hepatology    1527-3350    TITLE    0    0    246    LONGTERM|GENE|GN|DICTIONARY    12 15 lipoxygenase    12/15-lipoxygenase    18#36    NA    C0400966    LONGTERM|DISEASE|DT|DICTIONARY    nonalcoholic fatty liver disease    nonalcoholic fatty liver disease    85#117    NA    Disruption of the 12/15-lipoxygenase gene (Alox15) protects hyperlipidemic mice from nonalcoholic fatty liver disease.    18#36|43#49    85#117    
    
    ENT1_A = "€".decode('utf-8')
    ENT1_B = "$".decode('utf-8')
    ENT2_A = "¿".decode('utf-8')
    ENT2_B = "¬".decode('utf-8')
    
    
    ofile = open(output_path, "w")
    
    for lin in file(input_path):
        lin_split = lin.strip().split("\t")
        
        offset1 = lin_split[RESULT_COOC_ENTITY1_OFFSET]
        offset2 = lin_split[RESULT_COOC_ENTITY2_OFFSET]
        
        
        offset1_list = lin_split[RESULT_COOC_ENTITY1_OFFSET_LIST].split("|")
        offset2_list = lin_split[RESULT_COOC_ENTITY2_OFFSET_LIST].split("|")
        
        sent_prev = lin_split[RESULT_COOC_SENT_TEXT]
        sent = sent_prev.decode('utf-8')
        sent_2 = sent
        
        for off1 in offset1_list:
            ini = int(off1.split("#")[0])
            end = int(off1.split("#")[1])
            
            size = end - ini
            sent_ini = sent[:ini]
            sent_end = sent[end:]
            
            ent1 = "".join([ENT1_B]*size)
            if off1 == offset1:
                ent1 = "".join([ENT1_A]*size)
            sent = sent_ini + ent1 + sent_end 
            
            #sent2
            sent_2_ini = sent_2[:ini]
            sent_2_end = sent_2[end:]
            sent_2 = sent_2_ini + sent_2[ini:end].replace(" ", "_") + sent_2_end 
        
        for off2 in offset2_list:
            ini = int(off2.split("#")[0])
            end = int(off2.split("#")[1])
            
            size = end - ini
            sent_ini = sent[:ini]
            sent_end = sent[end:]
            
            ent2 = "".join([ENT2_B]*size)
            if off2 == offset2:
                ent2 = "".join([ENT2_A]*size)
            sent = sent_ini + ent2 + sent_end 
            
            #sent2
            sent_2_ini = sent_2[:ini]
            sent_2_end = sent_2[end:]
            sent_2 = sent_2_ini + sent_2[ini:end].replace(" ", "_") + sent_2_end
        
        sent = re.sub(ur'[€]+', ent1_A, sent)
        sent = re.sub(ur'[$]+', ent1_B, sent)
        sent = re.sub(ur'[¿]+', ent2_A, sent)
        sent = re.sub(ur'[¬]+', ent2_B, sent)
        
        if not ent1_A in sent or not ent2_A in sent:
            continue
        """
        sent_list = sent.replace("/", " ").replace("-", " ").replace(".", " ").split(" ")
        cont = 0
        for w in sent_list:
            if ent1_A in w:
                if ent1_B in w or ent2_A in w or ent2_B in w:
                    cont=1
                    break
            if ent2_A in w:
                if ent1_B in w or ent1_A in w or ent2_B in w:
                    cont=1
                    break
            if ent2_B in w:
                if ent1_B in w or ent1_A in w or ent2_A in w:
                    cont=1
                    break
            if ent1_B in w:
                if ent1_A in w or ent1_A in w or ent2_B in w:
                    cont=1
                    break
        """            
        lin_split.append(sent.encode('utf-8'))
        #if cont:
        #    print lin_split
        #    continue
        
        
        
        new_line = lin_split[0]
        for i in range(1, len(lin_split)):
            new_line = new_line + "\t" + lin_split[i]
        
        ofile.write(new_line + "\n")
                        
    ofile.close()   


if __name__ == '__main__':
    
    
    path = sys.argv[1]
    gene_filename = sys.argv[2]
    disease_filename = sys.argv[3]
    cooc_filename = sys.argv[4]

    """
    path = "/home/abravo/Documents/out2/"
    gene_filename = "genes_w16_FINAL.befree"
    disease_filename = "diseases_w16_FINAL.befree"
    cooc_filename = "cooc_w16"
    """
    print ""
    print get_re_process()
    print ""
    print "##### Co-occurrences Selection #####"
    start_time_total = time()
    
    gene_path = path + gene_filename +"_FINAL.befree"
    disease_path = path + disease_filename +"_FINAL.befree"
    
    step = ""
    cooc_path = path + cooc_filename + step +".befree"
    simple_coocurrence_selection(gene_path, 0, disease_path, 0, cooc_path)
    
    step += "_list"
    cooc_list_path = path + cooc_filename + step + ".befree"
    cooc_add_all_offsets_by_sentence(cooc_path, cooc_list_path)
    
    ent1_A = 'GeneA'
    ent1_B = 'GeneB'
    ent2_A = 'DiseaseA'
    ent2_B = 'DiseaseB'
    
    step += "_jsre"
    cooc_jsre_path = path + cooc_filename + step + ".befree"
    cooc_creation_2_jsre(cooc_list_path, cooc_jsre_path, ent1_A, ent1_B, ent2_A, ent2_B)
    
    #print get_results_screen()
    #print "  TOTAL TIME:"
    #print "   ", time() - start_time_total, "seconds"
    print "##### BeFree 2 JSRE format #####"
    
    
