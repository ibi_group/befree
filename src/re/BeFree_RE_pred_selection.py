# coding: utf-8
# encoding: utf-8
"""
    Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.

    BeFree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BeFree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to cite BeFree:
    Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
    Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

"""

import sys, glob
current_path = sys.path[0]
medline_path = "/".join(current_path.split("/")[:-1]) + "/ner"
sys.path.append(medline_path)


from BeFree_constants import RESULT_COOC_DOCID, RESULT_COOC_SENT_NUM, \
    RESULT_COOC_ENTITY1_OFFSET, RESULT_COOC_ENTITY2_OFFSET, \
    RESULT_COOC_ENTITY1_ID, RESULT_COOC_ENTITY2_ID


def get_valid_assoc_split(valid_assoc, jsre_path, pred_path):
    
    pred_list = open(pred_path).readlines()
    i=0
    for lin in file(jsre_path):
        
        if "1" in pred_list[i]:
            fields = lin.strip().split("\t")
            valid_assoc[fields[1]]=1
        i+=1
    return valid_assoc

def get_valid_assoc(jsre_path, pred_path):
    
    valid_assoc = {}
    pred_list = open(pred_path).readlines()
    
    i=0
    ok=0
    ko=0
    for lin in file(jsre_path):
        
        if "1" in pred_list[i]:
            fields = lin.strip().split("\t")
            valid_assoc[fields[1]]=1
            ok+=1
        else:
            ko+=1
        i+=1
    return valid_assoc
    
def statistics(cooc_jsre_path):
    
    assoc_dict = {}
    cooc_dict  = {}
    genes_dict = {}
    dis_dict={}
    pmids={}
    sent_dict={}
    i=0
    for lin in file(cooc_jsre_path):
        fields = lin.strip().split("\t")
        i+=1
        gene_id_list = fields[RESULT_COOC_ENTITY1_ID].split("|")
        dis_id_list = fields[RESULT_COOC_ENTITY2_ID].split("|")
        pmid = fields[RESULT_COOC_DOCID]
        nsent = fields[RESULT_COOC_SENT_NUM]
        pmids[pmid] = 1
        sent_dict[pmid+"-"+nsent]=1
        for g in gene_id_list:
            genes_dict[g]=1
            for d in dis_id_list:
                assoc_dict[g+"-"+d]=1
                dis_dict[d]=1
    print ""
    print "  FINAL Statistics:"    
    print "    ·Co-occurrences:", i
    print "    ·Associations:", len(assoc_dict)
    print "    ·Genes:", len(genes_dict)
    print "    ·Diseases:", len(dis_dict)
    print "    ·Publications:", len(pmids)
    print "    ·Sentences:", len(sent_dict)

def obtain_valid_assoc(cooc_path, valid_assoc, output_path):
    
    ofile = open(output_path, "w")
    for lin in file(cooc_path):
        fields = lin.strip().split("\t")
        pmid = fields[RESULT_COOC_DOCID]
        num_sent = fields[RESULT_COOC_SENT_NUM]
        offset1 = fields[RESULT_COOC_ENTITY1_OFFSET]
        offset2 = fields[RESULT_COOC_ENTITY2_OFFSET]
        key = "-".join([pmid, num_sent, offset1, offset2])
        if key in valid_assoc:
            ofile.write(lin)
    ofile.close

if __name__ == '__main__':
    
    cooc_path = sys.argv[1]
    jsre_path = sys.argv[2]
    #pred_path = sys.argv[3]
    cooc_final_path = sys.argv[3]
    
    #cooc_path = "/home/abravo/Documents/out2/cooc_w16_list_jsre.befree"
    #jsre_path = "/home/abravo/Documents/out2/cooc_w16_list_jsre.jsre"
    #pred_path = "/home/abravo/Documents/out2/cooc_w16_list_jsre.pred"
    #output_path = "/home/abravo/Documents/out2/cooc_w16_FINAL.befree"
    
    #valid_assoc = get_valid_assoc(jsre_path, pred_path)
    #obtain_valid_assoc(cooc_path, valid_assoc, cooc_final_path)
    #statistics(cooc_final_path)
    
    #cooc_path = "/home/abravo/Documents/out/cooc_w17_list_jsre.befree"
    #jsre_path = "/home/abravo/Documents/out/befree_split"
    #pred_path = "/home/abravo/Documents/out2/cooc_w16_list_jsre.pred"
    #cooc_final_path = "/home/abravo/Documents/out/cooc_w17_FINAL.befree"
    
    valid_assoc = {}
    for jsre_split_path in sorted(glob.glob(jsre_path+"*")):
        if jsre_split_path.split(".")[-1] == "pred":
            continue
        pred_path = jsre_split_path + ".pred"
        valid_assoc = get_valid_assoc_split(valid_assoc, jsre_split_path, pred_path)
    obtain_valid_assoc(cooc_path, valid_assoc, cooc_final_path)