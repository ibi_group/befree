#!/bin/bash

####################################################################
#
#	 Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.
#
#	 BeFree is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BeFree is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#	 How to cite BeFree:
#	 Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
#	 Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.
#
####################################################################

#######################
#     PARAMETERS      #
#######################

# Change these paths
DIRPATH="/home/.../befree"
ABSTRACTS_PATH="$DIRPATH/in/pubmed_result.xml"
DATABASE_NAME="medline"
COLLECTION_NAME="psychiatric_2010"
CLEAN_COLLECTION="y"


#######################
#     EXECUTION       #
#######################
#SCRIPT files
SCRIPT_PATH="$DIRPATH/src/medline/abstracts_storing.py"
JAR_PATH="$DIRPATH/src/medline/ExtractAbbrevFromMongoDB.jar"

PYTHON="python"
JAVA="java"

source "$DIRPATH/src/mongodb/mongodb_constants.py"

$PYTHON $SCRIPT_PATH $ABSTRACTS_PATH $FILE_TYPE $COLLECTION_NAME $DATABASE_NAME $CLEAN_COLLECTION
$JAVA -jar $JAR_PATH $DATABASE_NAME $COLLECTION_NAME $MONGODB_IP $MONGODB_PORT 