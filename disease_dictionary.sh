#!/bin/bash

####################################################################
#
#	Copyright (C) 2017 Àlex Bravo and Laura I. Furlong, IBI group.
#
#	BeFree is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	BeFree is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#	How to cite BeFree:
#	Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
#	Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.
#
####################################################################

#######################
#     PARAMETERS      #
#######################
DIRPATH="/home/.../befree"

#UMLS file
UMLS_PATH="$DIRPATH/in/dictionaryDiseaseCasper_tty.tsv"
UMLS_HEADER="y"

#BLACK list file
BLACKLIST_PATH="$DIRPATH/in/disease_cuis_blacklist.sample"
BLACKLIST_HEADER="n"

#SCRIPT files
UMLS_SCRIPT_PATH="$DIRPATH/src/dictionaries/disease_dictionary.py"

#######################
#     EXECUTION       #
#######################
PYTHON="python"
$PYTHON $UMLS_SCRIPT_PATH $UMLS_PATH $UMLS_HEADER $BLACKLIST_PATH $BLACKLIST_HEADER