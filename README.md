# BeFree: Bio-Entity Finder and RElation Extraction #

BeFree is a text mining tool that involves two main applications related to the IE task: (i) Named Entity Recognition (NER) and (ii) Relation Extraction (RE). BeFree is used in a text mining workflow aimed at extracting information on biological associations from scientific publications. Briefly, after document selection, the text mining approach comprises as a first step the recognition and normalization of the entities in biomedical publications by means of the BioNER module, and secondly, the identification of relationships between the aforementioned entities by their co-occurrence in sentences are processed by our RE module to predict the correct co-occurrences, that is, the correct associations.

### Getting Started ###

The documentation about whatever steps are necessary to get your application up and running are described in BeFree_Documentation.pdf.

### How to cite BeFree ###
* Bravo,À. et al. (2014) A knowledge-driven approach to extract disease-related biomarkers from the literature. Biomed Res. Int., 2014, 253128.
* Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

### Who do I talk to? ###

If you have questions or comments about BeFree contact us at: laura.furlong@upf.edu

Integrative Biomedical Informatics Group

Research Unit on Biomedical Informatics - GRIB

Barcelona Biomedical Research Park - PRBB

Dr. Aiguader 88,
08003 Barcelona.


email: laura.furlong@upf.edu

phone: +34 93 316 0521

fax: +34 93 316 0550

web: http://ibi.imim.es